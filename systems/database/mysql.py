from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate

def configDB(app):
    app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:muathuhanoi2014@localhost/ferosh'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    db = SQLAlchemy(app)
    migrate = Migrate(app, db)
    return db
