# -*- coding: utf-8 -*-    group_brand
from nltk.tokenize import SpaceTokenizer
from flask import jsonify
from datetime import datetime
import time
from elasticsearch import Elasticsearch
import json
import underthesea
from sys import platform
import pathlib
from sqlalchemy import null
from elasticsearch import helpers
import re
import statistics
from string import printable

class Search():
    def __init__(self):
        self.specialCharacters = ".,=+-_!;/()*\"&^:#|\n\t'"
        self.category_name_list = {}
        self.brand_name_list = {}
        self.size_list = {}
        self.color_name_list = {}
        self.es = Elasticsearch('localhost:9200')
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

        self.PATH_DIR = "\\"
        if platform == "linux" or platform == "linux2":
            self.PATH_DIR = "/"
        self.path = str(pathlib.Path(__file__).parent.absolute())
        self.update_dictionaries()

    def update_dictionaries(self):
        with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "tbl_colors.json", encoding="utf8") as f:
            data = json.loads(f.read())
            self.color_name_list.clear()
            for color in data:
                self.color_name_list[color["id"]] = {"cid": color["cid"], "name": color["name"],
                                                     "status": color["status"], "sub": color["sub"]}
        with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "tbl_brands.json", encoding="utf8") as f:
            data = json.loads(f.read())
            self.brand_name_list.clear()
            for brand in data:
                if brand["name"] != "" and brand["status"] == "Y":
                    self.brand_name_list[brand["id"]] = {"cid": brand["cid"], "name": brand["name"],
                                                         "status": brand["status"], "sub": brand["sub"], "alias": brand["alias"]}
        with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "sizes.json", encoding="utf8") as f:
            data = json.loads(f.read())
            for size in data:
                if size["name"] != "":
                    self.size_list[size["id"]] = {"origin_id": size["origin_id"], "name": size["name"]}
        with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "tbl_types.json", encoding="utf8") as f:
            data = json.loads(f.read())
            self.category_name_list.clear()
            for category in data:
                self.category_name_list[category["id"]] = {"cid": category["cid"], "sid": category["sid"], "alias": category["alias"],
                                                           "pid": category["pid"], "name": category["name"],
                                                           "status": category["status"], "sub": category["sub"]}

    def count_appear_times(self, tokens, search_string): # Word level counts
        length = len(tokens)
        if "'" in tokens and "s" in tokens and tokens.index("'") == (tokens.index("s") - 1):
            length = len(tokens) - 2
        if "." in tokens:
            length = len(tokens) - 1
        count = 0
        search_str = " " + search_string[0] + " "
        for i in tokens:
            temp_sides = " " + i + " "
            indx = tokens.index(i)
            if length < len(tokens):
                if i == "'" or i == "s":
                    continue
                if indx < len(tokens) - 2:
                    if tokens[indx + 1] == "'":
                        temp_sides = " " + i
                    if tokens[indx + 1] == ".":
                        temp_sides = " " + i
                if indx > 0:
                    if tokens[indx - 1] == ".":
                        temp_sides = i + " "
                    if tokens[indx - 1] == "s":
                        temp_sides = i + " "
            if (temp_sides in search_str) and len(i) >= 2:
                count += len(temp_sides.split())

        if count >= 0.5 * length:
            print("\n\ntokens: ", tokens)
            print("tokens length: ", length)
            print("search_string: ", search_string)
            print("count: ", count)
        return count, length

    def check_string_in_string(self, entity, search_string): # Whole string level check
        if str(type(search_string)) != "<class 'list'>":
            if not set(search_string).difference(printable):
                if entity != "":
                    #if entity.lower() in search_string.lower() or self.no_accent_vietnamese(entity.lower()) in search_string.lower() or search_string.lower() in entity.lower() or search_string.lower() in self.no_accent_vietnamese(entity.lower()):
                    if self.no_accent_vietnamese(entity.lower()) in self.no_accent_vietnamese(search_string.lower()) or self.no_accent_vietnamese(search_string.lower()) in self.no_accent_vietnamese(entity.lower()):
                        return True
            else:
                if entity != "":
                    if entity.lower() in search_string.lower() or search_string.lower() in entity.lower():
                        return True
        elif str(type(search_string)) == "<class 'list'>":
            for s in search_string:
                if s != "":
                    #if entity.lower() in search_string.lower() or self.no_accent_vietnamese(entity.lower()) in search_string.lower() or search_string.lower() in entity.lower() or search_string.lower() in self.no_accent_vietnamese(entity.lower()):
                    if self.no_accent_vietnamese(s.lower()) == self.no_accent_vietnamese(entity.lower()):
                        return True
        return False

    def detect_search_keywords(self, search_string):
        # 3 dicts below are used to stored entities that will be detected
        detected_color_list = {"color": []}
        detected_brand_list = {"brand": {}}
        detected_category_list = {"parent_type": {}, "child_type": {}}
        # Tokenize the search string:
        # tokenized_search_string = underthesea.word_tokenize(search_string.lower())
        tokenized_search_string = [search_string.lower()]
        # underthesea cannot tokenize well on 2-or-less-word words, so not tokenize if this meets:
        # if len(SpaceTokenizer().tokenize(search_string)) <= 2:
        #    tokenized_search_string = [search_string.lower()]
        check_diacritics = 1  # = 1 means search_string has diacritics, 0 otherwise
        count_appear = 0
        # loop through each category keyword in the predefined dictionary:
        # category is a dict, e.g: {"child_type": ["áo sơ mi", "ao so mi"]}, {"child_type": ["áo khoác", "ao khoac"]}
        no_color = False
        detected_categories = []
        for c_id, category in self.category_name_list.items():
            # tokenize keywords in the dictionary:
            if category["sub"] == 0:
                tokens = underthesea.word_tokenize(category["name"].lower())
            else:
                tokens = underthesea.word_tokenize(self.category_name_list[category["cid"]]["name"].lower())
            # count_appear: number of category name tokens that appear in the search string; length: modified tokens length in cases where special symbols like dots/ colons, etc. are in tokens
            count_appear, length = self.count_appear_times(tokens, tokenized_search_string) # Word level counts
            search_string_length = len(SpaceTokenizer().tokenize(search_string))
            # Make decision if count_appear and length and search_string_length are appropriate so the category is classified as detected (according to the required condition: >=0.67 bla bla):
            if (length <= 2 and count_appear >= 0.5 * length and ((count_appear >= 0.5 * search_string_length and search_string_length <= 2) or (count_appear >= 0.3 * search_string_length and search_string_length >2))) or (length == 3 and count_appear >= 0.6 * length and count_appear >= 0.3 * search_string_length) or (length > 3 and count_appear >= 0.67 * length and count_appear >= 0.2 * search_string_length):
                #if count_appear >= 0.5 * length:
                #    if count_appear >= 0.1 * search_string_length:
                if category["sub"] == 0: # if the current category in the dictionary is a main word
                    if category["cid"] == 0: # if the category is of parent type
                        if category["name"] not in detected_category_list["parent_type"].keys():
                            boost = 0
                            # Boost this category if it can match with the search string at the "whole-string" level: boost by string lengths ratio (0 to 1)
                            if self.check_string_in_string(category["name"], search_string):
                                boost += 2
                                if category["name"].lower() == search_string.lower():
                                    detected_categories.append(category["name"])
                                if self.check_string_in_string(category["name"].lower(), search_string.lower()):
                                    if len(category["name"]) <= len(search_string):
                                        boost += 2 * len(category["name"]) / len(search_string)
                                    else:
                                        boost += 2 * len(search_string) / len(category["name"])
                            # Add this category to the detected list if its final score is greater than 1
                            if (count_appear / length) + (count_appear/search_string_length) + boost > 1:
                                detected_category_list["parent_type"][category["name"]] = (count_appear / length) + (count_appear/search_string_length) + boost
                    else: # if the category is of child type
                        if category["name"] not in detected_category_list["child_type"].keys():
                            boost = 0
                            if self.check_string_in_string(category["name"], search_string):
                                boost += 2
                                if category["name"].lower() == search_string:
                                    detected_categories.append(category["name"])
                                if self.check_string_in_string(category["name"].lower(), search_string.lower()):
                                    if len(category["name"]) <= len(search_string):
                                        boost += 2 * len(category["name"]) / len(search_string)
                                    else:
                                        boost += 2 * len(search_string) / len(category["name"])
                            if (count_appear / length) + (count_appear/search_string_length) + boost > 1:
                                detected_category_list["child_type"][category["name"]] = (count_appear / length) + (count_appear/search_string_length) + boost
                        if category["cid"] == 86:
                            no_color = True
                else: # if the current category in the dictionary is a alternative word
                    #tokens = underthesea.word_tokenize(self.category_name_list[category["cid"]]["name"].lower())
                    if self.category_name_list[category["cid"]]["cid"] == 0:
                        if self.category_name_list[category["cid"]]["name"] not in detected_category_list["parent_type"].keys():
                            boost = 0
                            if self.check_string_in_string(category["name"], search_string.lower()):
                                boost += 2
                                if self.category_name_list[category["cid"]]["name"].lower() == search_string:
                                    detected_categories.append(self.category_name_list[category["cid"]]["name"])
                                if self.check_string_in_string(category["name"].lower(), search_string.lower()):
                                    if len(self.category_name_list[category["cid"]]["name"]) <= len(search_string):
                                        boost += 2 * len(self.category_name_list[category["cid"]]["name"]) / len(search_string)
                                    else:
                                        boost += 2 * len(search_string) / len(self.category_name_list[category["cid"]]["name"])
                                if (count_appear / len(tokens)) + (count_appear/search_string_length) + boost > 1:
                                    detected_category_list["parent_type"][self.category_name_list[category["cid"]]["name"]] = (count_appear / len(tokens)) + (count_appear/search_string_length) + boost
                        if category["cid"] == 86:
                            no_color = True
                    else:
                        if self.category_name_list[category["cid"]]["name"] not in detected_category_list["child_type"].keys():
                            boost = 0
                            if self.check_string_in_string(category["name"], search_string):
                                boost += 2
                                if self.category_name_list[category["cid"]]["name"].lower() == search_string:
                                    detected_categories.append(self.category_name_list[category["cid"]]["name"])
                                if self.check_string_in_string(category["name"].lower(), search_string.lower()):
                                    if len(self.category_name_list[category["cid"]]["name"]) <= len(search_string):
                                        boost += 2 * len(self.category_name_list[category["cid"]]["name"]) / len(search_string)
                                    else:
                                        boost += 2 * len(search_string) / len(self.category_name_list[category["cid"]]["name"])
                            if count_appear / len(tokens) + (count_appear/search_string_length) + boost > 1:
                                detected_category_list["child_type"][self.category_name_list[category["cid"]]["name"]] = count_appear / len(tokens) + (count_appear/search_string_length) + boost
                        if category["cid"] == 86:
                            no_color = True
                    check_diacritics = 0
        search_string_remade = ""  # this is another version of search_string, which will contain the detected entities, which is consistent with data in search_index
        chosen_category = ""  # this is used to store the final category after the filtering step below:
        chosen_category1 = ""
        chosen_category2 = ""
        max_count = 0
        max_count1 = 0
        max_count2 = 0
        # If the list of detected child types has less than 2 elements, e.g: search for "áo tay dài" -> detected" áo dài, áo dào tay, then the longer
        # ones, which are more specific, may be the correct category; and also, neglect parent types
        print("detected_categories list for exact matches: ", detected_categories)
        print("detected_category_list list: ", detected_category_list)
        if len(detected_categories) >= 0:
            if 0 < len(detected_category_list["child_type"]) <= 50:
                #detected_category_list["parent_type"] = {}
                mean_child = 0
                mean_parent = 0
                if len(detected_category_list["child_type"].values()) > 0:
                    mean_child = statistics.mean(detected_category_list["child_type"].values())
                if len(detected_category_list["parent_type"].values()) > 0:
                    mean_parent = statistics.mean(detected_category_list["parent_type"].values())
                if mean_child > mean_parent:
                    max_count = max(detected_category_list["child_type"].values())
                    for keyword, count in detected_category_list["child_type"].items():
                        print(f"\n\t\tMax count: {max_count}\tMean: {mean_child}\n\tFor keyword: {keyword} and its count: {count}")
                        if abs(count - max_count) <= 1.5:
                            if (count - mean_child >= 0 or abs(count - mean_child) <= 0.5):
                                chosen_category = keyword
                                max_count = count
                                if chosen_category not in detected_categories:
                                    detected_categories.append(chosen_category)
                    for keyword, count in detected_category_list["parent_type"].items():
                        if abs(count - max_count) <= 1.5:
                            if (count - mean_parent >= 0 or abs(count - mean_parent) <= 0.5):
                                chosen_category = keyword
                                max_count = count
                                if chosen_category not in detected_categories:
                                    detected_categories.append(chosen_category)
                else:
                    max_count = max(detected_category_list["parent_type"].values())
                    for keyword, count in detected_category_list["parent_type"].items():
                        print(f"\n\t\tMax count: {max_count}\tMean: {mean_parent}\n\tFor keyword: {keyword} and its count: {count}")
                        if abs(count - max_count) <= 1.5:
                            if (count - mean_parent >= 0 or abs(count - mean_parent) <= 0.5):
                                chosen_category = keyword
                                max_count = count
                                if chosen_category not in detected_categories:
                                    detected_categories.append(chosen_category)
                    for keyword, count in detected_category_list["child_type"].items():
                        if abs(count - max_count) <= 1.5:
                            if (count - mean_child >= 0 or abs(count - mean_child) <= 0.5):
                                chosen_category = keyword
                                max_count = count
                                if chosen_category not in detected_categories:
                                    detected_categories.append(chosen_category)
            else:
                """if len(detected_category_list["child_type"]) != 0:
                    std = statistics.stdev(detected_category_list["child_type"].values())
                    mean = statistics.mean(detected_category_list["child_type"].values())
                    print("===========o0o============")
                    print("\t", std)
                    print("\t", mean)
                    print("===========o0o============")
                    for keyword, count in detected_category_list["child_type"].items():
                        if count -  mean > std:
                            chosen_category = keyword
                            max_count = count
                            if chosen_category not in detected_categories:
                                detected_categories.append(chosen_category)"""
                if len(detected_categories) < 2:
                    # If the list of detected child types has more than 2 elements, this maybe the case that search_string is of general type, since a general
                    # type can be matched by a lot of specific types, e.g: search for "áo" -> detected child types: áo dài, áo sơ mi, áo lót, áo
                    # khoác ---> SOLUTION: detect parent types only (also choose the longest type)
                    for keyword, count in detected_category_list["parent_type"].items():
                        if count >= max_count1:
                            chosen_category1 = keyword
                            max_count1 = count
                    for keyword, count in detected_category_list["child_type"].items():
                        if count >= max_count2:
                            chosen_category2 = keyword
                            max_count2 = count
                    print("\n======\nmax count1: ", max_count1)
                    print("chosen 1: ", chosen_category1)
                    print("\n======\nmax count1: ", max_count2)
                    print("chosen 1: ", chosen_category2)
                    if max_count1 > 0.9 * max_count2 and max_count2 > 0.9 * max_count1: # if both highest cat in parent and child cat list, append both of them if their scores are approxiately the same; else: choose the cat whose higher max score
                        if chosen_category1 not in detected_categories:
                            detected_categories.append(chosen_category1)
                        if chosen_category2 not in detected_categories:
                            detected_categories.append(chosen_category2)
                    elif max_count2 > 0.9 * max_count1:
                        if chosen_category2 not in detected_categories:
                            detected_categories.append(chosen_category2)
                    else:
                        if chosen_category1 not in detected_categories:
                            detected_categories.append(chosen_category1)
        # if any color in the dictionary appears in the search_string, then append it in detected_color_list
        for key, value in self.category_name_list.items():
            if search_string.lower() == value["name"].lower():
                if value["sub"] == 0:
                    detected_categories = [value["name"]]
                else:
                    detected_categories = [self.category_name_list[value["cid"]]["name"]]
        if no_color:
            detected_color_list["color"] = ""
        else:
            for co_id, color in self.color_name_list.items():
                tokens = underthesea.word_tokenize(color["name"].lower())
                count_appear, length = self.count_appear_times(tokens, tokenized_search_string)
                if count_appear >= 0.67 * length:
                    if color["sub"] == 0:
                        if color["name"] not in detected_color_list["color"]:
                            detected_color_list["color"].append(color["name"])
                    else:
                        if self.color_name_list[color["cid"]]["name"] not in detected_color_list["color"]:
                            detected_color_list["color"].append(self.color_name_list[color["cid"]]["name"])
        # if any brand in the dictionary appears in the search_string, then append it the detected_color_brand
        for b_id, brand in self.brand_name_list.items():
            tokens = SpaceTokenizer().tokenize(brand["name"].lower())
            tokens_length = len(tokens)
            if brand["name"].lower() == "seven.am":
                tokens = ["seven", ".", "am"]
            count_appear, length = self.count_appear_times(tokens, tokenized_search_string)
            if length <= 2:
                if count_appear > 0.5 * length:
                    if count_appear >= 0.1 * len(SpaceTokenizer().tokenize(search_string)):
                        if brand["sub"] == 0:
                            if brand["name"] not in detected_brand_list["brand"].keys():
                                detected_brand_list["brand"][brand["name"]] = count_appear/length
                        else:
                            if self.brand_name_list[brand["cid"]]["name"] not in detected_brand_list["brand"]:
                                detected_brand_list["brand"][self.brand_name_list[brand["cid"]]["name"]] = count_appear/length
            elif length == 3:
                if count_appear >= 0.6 * length:
                    if count_appear >= 0.3 * len(SpaceTokenizer().tokenize(search_string)):
                        if brand["sub"] == 0:
                            if brand["name"] not in detected_brand_list["brand"].keys():
                                detected_brand_list["brand"][brand["name"]] = count_appear/length
                        else:
                            if self.brand_name_list[brand["cid"]]["name"] not in detected_brand_list["brand"]:
                                detected_brand_list["brand"][self.brand_name_list[brand["cid"]]["name"]] = count_appear/length
            elif count_appear >= 0.67 * length:
                if count_appear >= 0.3 * len(SpaceTokenizer().tokenize(search_string)):
                    if brand["sub"] == 0:
                        if brand["name"] not in detected_brand_list["brand"].keys():
                            detected_brand_list["brand"][brand["name"]] = count_appear/length
                    else:
                        if self.brand_name_list[brand["cid"]]["name"] not in detected_brand_list["brand"]:
                            detected_brand_list["brand"][self.brand_name_list[brand["cid"]]["name"]] = count_appear/length
        max_count = 0
        chosen_brand = ""
        print("original brand list: ", detected_brand_list)
        for keyword, count in detected_brand_list["brand"].items():
            if count >= max_count:
                chosen_brand = keyword
                max_count = count
        print("color list: ", detected_color_list)
        print("brand list: ", chosen_brand)
        print("final category chosen: ", detected_categories)
        return detected_color_list, chosen_brand, detected_categories, search_string_remade, check_diacritics

    def generateJsonDocSearch(self, page, limit, chosen_brand = None, chosen_cat = None, detected_entities = None, search_string = None, chosen_size = None, chosen_price_range=None, searchsuggest=False, reversing=None):
        # calculating the position (in result set) of the document at page number page
        offset = (page - 1) * limit
        json_docs = {
            "from": offset, "size": limit,
            "query": {
                "bool": {
                    "filter": [
                        {
                        "bool": {
                            "should": []
                        }
                        }
                    ],
                    "must": [{"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}},
                        {"match": {"status": "Y"}},
                        {"range": {"inventory": {"gte": 0}}},
                        {"bool":
                            {"should": [
                                {"match": {"tag": "3d"}},
                                {"match": {"tag": "collection"}}
                            ]
                            }
                        }
                    ],
                    "should": [], "must_not": []
                }
            },
            "aggregations": {
                "group_brand": {"terms": {"field": "brand_display", "size": 100}},
                "group_category": {"terms": {"field": "category_display", "size": 100}},
                "group_sizes": {"terms": {"field": "sizes.raw", "size": 100}},
                "max_prices": {"max": {"field": "price"}},
                "min_prices": {"min": {"field": "price"}}
            }
        }
        if reversing == "asc":
            json_docs["sort"] = {
                "_script": {
                    "type": "number",
                    "order": "asc",
                    "script": {
                        "inline": "long special_to_date = doc['special_to_date'].value; long special_from_date = doc['special_from_date'].value; long flash_sale_to_date = doc['flash_sale_to_date'].value; long flash_sale_from_date = doc['flash_sale_from_date'].value; long timestampNow = new Date().getTime(); long special_price = doc['price'].value; if (special_to_date > timestampNow && special_from_date < timestampNow) special_price = doc['special_price'].value; if (flash_sale_to_date > timestampNow && flash_sale_from_date < timestampNow) special_price = doc['flash_sale_price'].value; if (special_price == 0) return doc['price'].value; return special_price;"
                    }
                }
            }
        elif reversing == "desc":
            json_docs["sort"] = {
                "_script": {
                    "type": "number",
                    "order": "desc",
                    "script": {
                        "inline": "long special_to_date = doc['special_to_date'].value; long special_from_date = doc['special_from_date'].value; long flash_sale_to_date = doc['flash_sale_to_date'].value; long flash_sale_from_date = doc['flash_sale_from_date'].value; long timestampNow = new Date().getTime(); long special_price = doc['price'].value; if (special_to_date > timestampNow && special_from_date < timestampNow) special_price = doc['special_price'].value; if (flash_sale_to_date > timestampNow && flash_sale_from_date < timestampNow) special_price = doc['flash_sale_price'].value; if (special_price == 0) return doc['price'].value; return special_price;"
                    }
                }
            }
        # Convert chosen entities alias to their real name using their dictionaries
        if len(chosen_brand) != 0 and chosen_brand[0] != "":
            for b in chosen_brand:
                for key, value in self.brand_name_list.items():
                    if value["alias"] == b:
                        chosen_brand[chosen_brand.index(b)] = value["name"]
                        b = value["name"]
                        break
                json_docs["query"]["bool"]["must"][0]["bool"]["should"].append({"match": { "brand_display": { "query": "" + b + "", "operator": "and"}}})
        if len(chosen_cat) != 0 and chosen_cat[0] != "":
            exclude = False
            parent_indices = []
            for c in chosen_cat: # check if any parent type cat is in the chosen cat list
                for key, value in self.category_name_list.items():
                    if value["alias"] == c:
                        chosen_cat[chosen_cat.index(c)] = value["name"]
                        c = value["name"]
                        if value["cid"] != 0:
                            exclude = True
                        else:
                            parent_indices.append(chosen_cat.index(c))
                        break
            if exclude: # exclude = Ture means there is at least 1 child cat ---> pop out all parent cat, search will be performed upon child cats only
                for i in parent_indices:
                    chosen_cat.pop(i)
            parent_id = 0
            for c in chosen_cat:
                json_docs["query"]["bool"]["must"][1]["bool"]["should"].append({"match": { "category_display": { "query": "" + c + "", "operator": "and"}}})
                if not exclude:
                    for key, value in self.category_name_list.items():
                        if value["name"] == c:
                            parent_id = key
                            break
                    for key, value in self.category_name_list.items():
                        if value["cid"] == parent_id:
                            json_docs["query"]["bool"]["must"][1]["bool"]["should"].append({"match": { "category_display": { "query": "" + value["name"] + "", "operator": "and"}}})
        if len(chosen_size) != 0 and chosen_size[0] != "":
            for s in chosen_size:
                for key, value in self.size_list.items():
                    if value["origin_id"] == int(s):
                        s = value["name"]
                        break
                json_docs["query"]["bool"]["must"][2]["bool"]["should"].append({"match": { "sizes": { "query": "" + s + ""}}})
        if len(chosen_price_range) != 0 and chosen_price_range[0] != "":
            json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"range": { "price": { "gte": chosen_price_range[0], "lte": chosen_price_range[1]}}})
        # This corresponds to "apply filter on searchproduct results"
        print("\n=============\n\t\tDETECTED ENTITIES\n===============\n", detected_entities)
        search_string_for_es = {"search_string": search_string, "filter_color": [], "filter_brand": []}
        search_string_for_es["filter_color"].append(detected_entities[0]["color"])
        search_string_for_es["filter_brand"].append(detected_entities[1])
        search_string_for_es["filter_category"] = detected_entities[2]
        json_docs["query"]["bool"]["should"].append(
                    {
                            "match": {
                                "name_analyze.searching": {
                                    "query": "" + search_string_for_es["search_string"] + "", "boost": 5,
                                }
                            }
                    })
        json_docs["query"]["bool"]["should"].append(
                        {
                            "match": {
                                "description": {
                                    "query": "" + search_string_for_es["search_string"] + "", "boost": 5
                                }
                            }
                        })
        if len(search_string_for_es["filter_brand"][0]) != 0:
            json_docs["query"]["bool"]["must"][3]["bool"]["should"].append(
                {"match": {"brand_display": {"query": "" + search_string_for_es["filter_brand"][0] + "", "operator": "and"}}})
        if len(search_string_for_es["filter_category"]) != 0:
            if len(chosen_cat) == 0 or chosen_cat[0] == "":
                for c in search_string_for_es["filter_category"]:
                    if c == "":
                        continue
                    json_docs["query"]["bool"]["must"][1]["bool"]["should"].append({"match": { "category_display": { "query": "" + c + "", "operator": "and"}}})
                    parent_id = -1
                    for key, value in self.category_name_list.items():
                        if value["name"] == c:
                            if value["cid"] == 0:
                                parent_id = key
                            break
                    if parent_id != -1:
                        for key, value in self.category_name_list.items():
                            if value["cid"] == parent_id and value["sub"] != 1:# and value["name"] in search_string_for_es["filter_category"]:
                                json_docs["query"]["bool"]["must"][1]["bool"]["should"].append({"match": { "category_display": { "query": "" + value["name"] + "", "operator": "and"}}})
            else:
                for i in chosen_cat: # chosen_cat now will only store child cats, all parents have been popped out
                    if i != "": # append to must either at index 4 (dedicated for detected) or 1 (dedicated for chosen cats)
                        json_docs["query"]["bool"]["must"][1]["bool"]["should"].append({"match": {"category_display": {"query": "" + i + "", "operator": "and"}}})
        if len(search_string_for_es["filter_color"][0]) != 0:
            json_docs["query"]["bool"]["should"].append(
                {"match": {"description": {"query": "" + search_string_for_es["filter_color"][0][0] + "", "boost": 5}}})
        if len(search_string_for_es["filter_brand"][0]) == 0 and len(search_string_for_es["filter_category"]) == 1 and search_string_for_es["filter_category"][0] == "":
            json_docs["query"]["bool"]["minimum_should_match"] = "1"
        print("Search product/ Filter searchproduct results:\n\n", json_docs)
        
        return json_docs

    def searching(self, json_docs, index_name):
        results = self.es.search(body=json_docs, index=index_name)
        max_score = results["hits"]["max_score"]  # highest score in the result set
        num = results["hits"]["total"]  # number of products found (bounded by limit)
        print("*************num is ", num)
        time_taken = results["took"]  # time spent on searching
        return results, num, time_taken, max_score

    def return_products(self, search_results, num, time_taken, reversing=None):
        products = {"total": num, "time": time_taken, "products": []}
        len1 = 0
        len2 = 0
        if "group_brand" in search_results["aggregations"].keys():
            products["group_brand"] = search_results["aggregations"]["group_brand"]["buckets"]
            len1 = len(products["group_brand"])
        if "group_category" in search_results["aggregations"].keys():
            products["group_category"] = search_results["aggregations"]["group_category"]["buckets"]
            len2 = len(products["group_category"])
        if "group_sizes" in search_results["aggregations"].keys():
            products["group_sizes"] = search_results["aggregations"]["group_sizes"]["buckets"]
        for sugg in range(0, len1):
            for key, value in self.brand_name_list.items():
                if value["name"] == products["group_brand"][sugg]["key"]:
                    products["group_brand"][sugg]["alias"] = value["alias"]
        hierachical_cats = []
        parent_cat = {"cat": "", "alias": "", "childs": [], "doc_count": 0}
        child_cat = {"cat": "", "alias": "", "doc_count": 0}
        for sugg in range(0, len2):
            for key, value in self.category_name_list.items():
                if value["name"] == products["group_category"][sugg]["key"]:
                    if value["cid"] == 0:
                        if not any(temp["cat"] == value["name"] for temp in hierachical_cats):
                            hierachical_cats.append({"cat":value["name"],"alias":value["alias"],"childs":[],"doc_count":products["group_category"][sugg]["doc_count"]})
                    if value["cid"] != 0:
                        if not any(temp["cat"] == self.category_name_list[value["cid"]]["name"] for temp in hierachical_cats):
                            hierachical_cats.append({"cat":self.category_name_list[value["cid"]]["name"],"alias":self.category_name_list[value["cid"]]["alias"],"childs":[],"doc_count":products["group_category"][sugg]["doc_count"]})
        hierachy_length = len(hierachical_cats)
        for sugg in range(0, len2):
            for key, value in self.category_name_list.items():
                if value["name"] == products["group_category"][sugg]["key"] and value["cid"] != 0:
                    for i in range(0, hierachy_length):
                       if hierachical_cats[i]["cat"] == self.category_name_list[value["cid"]]["name"]:
                           hierachical_cats[i]["childs"].append({"cat":value["name"],"alias":value["alias"],"doc_count":products["group_category"][sugg]["doc_count"]})
                           break
        products["group_category"] = hierachical_cats
        size_bucket = []
        s = {}
        for sizes in products["group_sizes"]:
            split_size = sizes["key"].split()
            count = 0
            size_len = len(split_size)
            for size in split_size:
                if count <= size_len - 1:
                    if size.lower() == "free":# and split_size[split_size.index(size) + 1].lower() == "size":
                        if not any("free size" == bucket["key"].lower() for bucket in size_bucket):
                            # Uncomment this when size dictionaries is available
                            for key, value in self.size_list.items():
                                if value["name"].lower() == "free size":
                                    s = {"key": value["name"], "id": value["origin_id"]}
                                    break
                            size_bucket.append(s)
                    elif size.lower() == "đặt":# and split_size[split_size.index(size) + 1].lower() == "may":
                        if not any("đặt may" == bucket["key"].lower() for bucket in size_bucket):
                            # Uncomment this when size dictionaries is available
                            for key, value in self.size_list.items():
                                if value["name"].lower() == "đặt may":
                                    s = {"key": value["name"], "id": value["origin_id"]}
                                    break
                            size_bucket.append(s)
                    elif size.lower() == "một":# and split_size[split_size.index(size) + 1].lower() == "may":
                        if not any("một cỡ" == bucket["key"].lower() for bucket in size_bucket):
                            for key, value in self.size_list.items():
                                if value["name"].lower() == "một cỡ":
                                    s = {"key": value["name"], "id": value["origin_id"]}
                                    break
                            size_bucket.append(s)
                    elif size.lower() == "size" or size.lower() == "may" or size.lower() == "cỡ":
                        continue
                    elif not any(size.lower() == bucket["key"].lower() for bucket in size_bucket):
                        for key, value in self.size_list.items():
                            if value["name"].lower() == size.lower():
                                s = {"key": size, "id": value["origin_id"]}
                                break
                        size_bucket.append(s)
                count += 1
        products["group_sizes"] = size_bucket
        # form products dictionary to put into log_action_index
        for res in search_results["hits"]["hits"]:
            timestamp = int(time.time())
            special_price = res['_source']['price']
            images = {}
            if res['_source']['images'] != "":
                images = json.loads(res['_source']['images'])
            if 'sort' in res.keys():
                products["products"].append(
                    {"name": res['_source']['name_display'],
                     "id": res['_source']['id'],
                     "brand": res['_source']['brand_display'],
                     "alias": res['_source']['alias'],
                     "price": res['_source']['price'],
                     "images": images,
                     "final_special_price": res['sort'][0]})
            else:
                special_to_timestamp = int(time.mktime(datetime.strptime(res["_source"]["special_to_date"], "%Y-%m-%d %H:%M:%S").timetuple()))
                flash_sale_to_timestamp = int(time.mktime(datetime.strptime(res["_source"]["flash_sale_to_date"], "%Y-%m-%d %H:%M:%S").timetuple()))
                if special_to_timestamp >= timestamp:
                    special_price = res["_source"]["special_price"]
                if flash_sale_to_timestamp >= timestamp:
                    special_price = res["_source"]["flash_sale_price"]

                products["products"].append(
                    {"name": res['_source']['name_display'],
                     "id": res['_source']['id'],
                     "brand": res['_source']['brand_display'],
                     "alias": res['_source']['alias'],
                     "price": res['_source']['price'],
                     "images": images,
                     "final_special_price": special_price})
        return products

    def add_products(self, products, search_id, search_string, detected):
        # Add products into the corresponding search_id in log_action_index
        # "total_views": 0, "total_clicks": 0, "total_orders": 0 --> initially there's no interactions with the result pages yet
        search_in = {"id": search_id, "string": search_string, "total_views": 0, "total_clicks": 0, "total_orders": 0}
        date_time_in = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        query = {
            "doc": {
                "search_id": search_in["id"],
                "search_string": search_in["string"].lower(),
                "created_at": date_time_in,
                "detected": {
                    "color": detected[0]["color"],
                    "brand": detected[1],
                    "category": detected[2]
                },
                "total_views": search_in["total_views"],
                "total_clicks": search_in["total_clicks"],
                "total_orders": search_in["total_orders"]#,
            },
            "doc_as_upsert": True
        }
        # add search basic information to log_action_index: search id, search string, detected entities, timestamp:
        self.es.update(doc_type="log_action", index="log_action_index", body=query, id=search_in["id"])

    def suggest_1(self, page=1, limit=0, index_name=None, string=None, detected_entities=None, searchsuggest=False, chosen_brand=None, chosen_size=None, chosen_cat=None, chosen_price_range=None, first_chosen=None, reversing=None):
        offset = (page - 1) * limit
        json_docs = {
            "from": offset,
            "size": limit,
            "query": {
                "bool": {
                    "must": [
                        {"bool":{"should": []}},
                        {"match": {"status": "Y"}},
                        {"range": {"inventory": {"gte": 0}}},
                        {"bool": 
                            {"should": [
                                {"match": {"tag": "3d"}},
                                {"match": {"tag": "collection"}}
                            ]
                            }
                        },
                        {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}}, {"bool": {"should": []}},
                    ],
                    "must_not": []
                }
            },
            "aggregations": {
                "group_sizes":{"terms": {"field": "sizes.raw", "size": 100}},
                "max_prices": {"max": {"field": "price"}},
                "min_prices": {"min": {"field": "price"}}
            }
        }
        if reversing == "asc":
            json_docs["sort"] = {                                                                                                                                                         "_script": {                                                                                                                                                                  "type": "number",                                                                                                                                                         "order": "asc",
                    "script": {
                        "inline": "long special_to_date = doc['special_to_date'].value; long special_from_date = doc['special_from_date'].value; long flash_sale_to_date = doc['flash_sale_to_date'].value; long flash_sale_from_date = doc['flash_sale_from_date'].value; long timestampNow = new Date().getTime(); long special_price = doc['price'].value; if (special_to_date > timestampNow && special_from_date < timestampNow) special_price = doc['special_price'].value; if (flash_sale_to_date > timestampNow && flash_sale_from_date < timestampNow) special_price = doc['flash_sale_price'].value; if (special_price == 0) return doc['price'].value; return special_price;"
                    }
                }
            }
        elif reversing == "desc":
            json_docs["sort"] = {
                "_script": {                                                                                                                                                                  "type": "number",                                                                                                                                                         "order": "desc",                                                                                                                                                          "script": {                                                                                                                                                                   "inline": "long special_to_date = doc['special_to_date'].value; long special_from_date = doc['special_from_date'].value; long flash_sale_to_date = doc['flash_sale_to_date'].value; long flash_sale_from_date = doc['flash_sale_from_date'].value; long timestampNow = new Date().getTime(); long special_price = doc['price'].value; if (special_to_date > timestampNow && special_from_date < timestampNow) special_price = doc['special_price'].value; if (flash_sale_to_date > timestampNow && flash_sale_from_date < timestampNow) special_price = doc['flash_sale_price'].value; if (special_price == 0) return doc['price'].value; return special_price;"
                    }                                                                                                                                                                     }                                                                                                                                                                     }
        cat_brand_contains = {}
        cat_contains = {}
        brand_contains = {}
        # Convert chosen alias to their real names using their dictionaries
        if searchsuggest:
            if first_chosen:
                for key, value in self.brand_name_list.items():
                    if value["alias"] == first_chosen:
                        first_chosen = value["name"]
                        break
                for key, value in self.category_name_list.items():
                    if value["alias"] == first_chosen:
                        first_chosen = value["name"]
                        break
            if chosen_brand:
                if len(chosen_brand) != 0 and chosen_brand[0] != "":
                    for b in chosen_brand:
                        for key, value in self.brand_name_list.items():
                            print(value)
                            if value["alias"] == b:
                                chosen_brand[chosen_brand.index(b)] = value["name"]
                                b = value["name"]
                                break
            if chosen_cat:
                if len(chosen_cat) != 0 and chosen_cat[0] != "":
                    for c in chosen_cat:
                        for key, value in self.category_name_list.items():
                            if value["alias"] == c:
                                chosen_cat[chosen_cat.index(c)] = value["name"]
                                c = value["name"]
                                break
            if chosen_size:
                if len(chosen_size) != 0 and chosen_size[0] != "":
                    for s in chosen_size:
                        for key, value in self.size_list.items():
                            if value["origin_id"] == int(s):
                                s = value["name"]
                                break
        low = string.lower()
        if detected_entities[1] != "" or (len(detected_entities[2]) != 0 and detected_entities[2][0] != ""):
            # Add to the query the detected brands
            if detected_entities[1] != "":
                json_docs["query"]["bool"]["must"][4]["bool"]["should"].append({"match": {"brand_display": {"query": detected_entities[1], "operator": "and"}}})
                # json_docs 1 is the query after appending brands
                print("\n===\nDETECTED: json docs 1 brand contain is: ", json_docs)
            if len(detected_entities[2]) != 0:
                detected_cats = []
                all_child = True
                for c in detected_entities[2]:
                    if c == "":
                        continue
                    for key, value in self.category_name_list.items():
                        if value["name"] == c:
                            if value["cid"] == 0:
                                all_child = False
                            break
                    if not all_child:
                        break
                if all_child:
                    l = len(detected_entities[2])
                    for c in range(0, l):
                        detected_cats.append(detected_entities[2][c])
                        for key, value in self.category_name_list.items():
                            if value["name"] == detected_entities[2][c]:
                                if self.category_name_list[value["cid"]]["name"] not in detected_cats:
                                    detected_cats.append(self.category_name_list[value["cid"]]["name"])
                                break
                for c in detected_entities[2]:
                    if c == "":
                        continue
                    json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + c + "", "operator": "and"}}})
                    parent_id = -1
                    for key, value in self.category_name_list.items():
                        if value["name"] == c:
                            parent_id = key
                            break
                    if parent_id != -1:
                        for key, value in self.category_name_list.items():
                            if value["cid"] == parent_id:
                                json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + value["name"] + "", "operator": "and"}}})
                # json_docs 2 is the query after appending categories
                print("\n===\nDETECTED: json docs 2 category contain is: ", json_docs)
            json_docs["aggregations"]["group_brand"] = {"terms": {"field": "brand_display", "size": 100}}
            json_docs["aggregations"]["group_category"] = {"terms": {"field": "category_display", "size": 100}}

            # json_docs 3 is the query after appending aggregations for brands and categories
            print("\n===\nDETECTED: json docs 3 category contain is: ", json_docs)
            print("\n===\nDETECTED ENTITES\n===", detected_entities)
            cat_brand_contains = self.es.search(body=json_docs, index=index_name)
            print("\n\t************cat_brand_contains BEFORE adding chosen stuff**********\n", cat_brand_contains["aggregations"]["group_brand"]["buckets"])
            # Add chosen filters in:
            if searchsuggest:
                print(f"=============\n*\n*\n* chosen brand: {chosen_brand}\n*\n*\n=============\n")
                print(f"=============\n*\n*\n* chosen cat: {chosen_cat}\n*\n*\n=============\n")
                print("cat_brand_contains cat buckets: \n", cat_brand_contains["aggregations"]["group_category"]["buckets"])
                if any((self.check_string_in_string(low_brand.lower(), chosen_brand) and low_brand != "") for low_brand in [detected_entities[1]]) or any((self.check_string_in_string(low_cat.lower(), chosen_cat) and low_cat != "") for low_cat in detected_cats) or any((self.check_string_in_string(low_brand["key"].lower(), chosen_brand) and low_brand["key"] != "") for low_brand in cat_brand_contains["aggregations"]["group_brand"]["buckets"]) or any((self.check_string_in_string(low_cat["key"].lower(), chosen_cat) and low_cat["key"] != "") for low_cat in cat_brand_contains["aggregations"]["group_category"]["buckets"]):
                    if chosen_brand:
                        if len(chosen_brand) != 0 and chosen_brand[0] != "":
                            for b in chosen_brand:
                                json_docs["query"]["bool"]["must"][7]["bool"]["should"].append({"match": { "brand_display": { "query": "" + b + "", "operator": "and"}}})
                    if len(chosen_cat) != 0 and chosen_cat[0] != "":
                        json_docs["query"]["bool"]["must"][5]["bool"]["should"] = []
                        print("***************\n***************\n***************\n***************\n***************\n", chosen_cat)
                        print("***************\n***************\n***************\n***************\n***************\n", first_chosen)
                        print("***************\n***************\n***************\n***************\n***************\n")
                        exclude = False
                        parent_indices = []
                        for c in chosen_cat:
                            for key, value in self.category_name_list.items():
                                if value["name"] == c:
                                    if value["cid"] != 0:
                                        exclude = True
                                    else:
                                        parent_indices.append(chosen_cat.index(c))
                                    break
                        if exclude:
                            for i in parent_indices:
                                chosen_cat.pop(i)
                        parent_id = 0
                        for c in chosen_cat: # already cut down all parent cats if any child cat is found in chosen_cat
                            json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + c + "", "operator": "and"}}})
                            if not exclude:
                                print("***************\n***************\n***************\n***************\n***************\nIN IF 11111111111")
                                for key, value in self.category_name_list.items():
                                    if value["name"] == c:
                                        parent_id = key
                                        break
                                for key, value in self.category_name_list.items():
                                    if value["cid"] == parent_id:
                                        json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + value["name"] + "", "operator": "and"}}})
                                break # There will only be ONE AND ONLY ONE parent cat in a chosen cat list, and the cat list definitely stores only cats of a category family, and child cats are already added in the if not exclude condition --> no need to continue the loop
                    if chosen_size:
                        if len(chosen_size) != 0 and chosen_size[0] != "":
                            for s in chosen_size:
                                for key, value in self.size_list.items():
                                    if value["origin_id"] == int(s):
                                        s = value["name"]
                                        break
                                json_docs["query"]["bool"]["must"][6]["bool"]["should"].append({"match": { "sizes": { "query": "" + s + ""}}})
                    if chosen_price_range:
                        if len(chosen_price_range) != 0 and chosen_price_range[0] != "":
                            json_docs["query"]["bool"]["must"][9]["bool"]["should"].append({"range": { "price": { "gte": int(chosen_price_range[0]), "lte": int(chosen_price_range[1])}}})
                    print("\n\t************cat_brand_contains AFTER chosen stuff**********\n", json_docs)
                    json_docs["query"]["bool"]["should"] = {"match": {"name_analyze.suggesting": {"query": low}}}
                    cat_brand_contains = self.es.search(body=json_docs, index=index_name)
                    print("IN HEREEE DUDEEEEEEEEEEEE")
                    print("chosen_brand", chosen_brand)
                    print("chosen_cat", chosen_cat)
                    print("low", low)
                    return cat_brand_contains
        else:
            json_docs["aggregations"]["group_brand"] = {"terms": {"field": "brand_display", "size": 100}}
            json_docs["query"]["bool"]["must"].append({"match": {"brand_analyze.suggesting": {"query": string, "operator": "and"}}})
            brand_contains = self.es.search(body=json_docs, index=index_name)
            # json_docs 1 is the query for brands that match
            print("\n\t************BRAND_CONTAINS BEFORE chosen stuff**********\n", json_docs)
            # chosen brand can be of this query, so add chosen filters and return if so
            if searchsuggest:
                # brand_contains's brand bucket keys are displayed on the suggestion bar, so if chosen one is in this bucket, results of this query + chosen must be returned
                if any(self.check_string_in_string(low_brand["key"].lower(), chosen_brand) and low_brand["key"] != "" for low_brand in brand_contains["aggregations"]["group_brand"]["buckets"]):
                    print("*******************\tBRAND_CONTAINS brand bucket:", brand_contains["aggregations"]["group_brand"]["buckets"])
                    if len(chosen_brand) != 0 and chosen_brand[0] != "":
                        for b in chosen_brand:
                            json_docs["query"]["bool"]["must"][7]["bool"]["should"].append({"match": { "brand_display": { "query": "" + b + "", "operator": "and"}}})
                    if len(chosen_cat) != 0 and chosen_cat[0] != "":
                        exclude = False
                        parent_indices = []
                        for c in chosen_cat:
                            for key, value in self.category_name_list.items():
                                if value["name"] == c:
                                    if value["cid"] != 0:
                                        exclude = True
                                    else:
                                        parent_indices.append(chosen_cat.index(c))
                                    break
                        if exclude:
                            for i in parent_indices:
                                chosen_cat.pop(i)
                        parent_id = 0
                        for c in chosen_cat:
                            json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + c + "", "operator": "and"}}})
                            if not exclude:
                                for key, value in self.category_name_list.items():
                                    if value["name"] == c:
                                        parent_id = key
                                        break
                                for key, value in self.category_name_list.items():
                                    if value["cid"] == parent_id:
                                        json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + value["name"] + "", "operator": "and"}}})
                    if len(chosen_size) != 0 and chosen_size[0] != "":
                        for s in chosen_size:
                            for key, value in self.size_list.items():
                                if value["origin_id"] == int(s):
                                    s = value["name"]
                                    break
                            json_docs["query"]["bool"]["must"][6]["bool"]["should"].append({"match": { "sizes": { "query": "" + s + ""}}})
                    if chosen_price_range:
                        if len(chosen_price_range) != 0 and chosen_price_range[0] != "":
                            json_docs["query"]["bool"]["must"][9]["bool"]["should"].append({"range": { "price": { "gte": int(chosen_price_range[0]), "lte": int(chosen_price_range[1])}}})
                    json_docs["aggregations"]["group_category"] = {"terms": {"field": "category_display", "size": 100}}
                    print("\n\t************BRAND_CONTAINS AFTER chosen stuff**********\n", json_docs)
                    brand_contains = self.es.search(body=json_docs, index=index_name)
                    return brand_contains
                
            json_docs["aggregations"].pop("group_brand")
            json_docs["aggregations"]["group_category"] = {"terms": {"field": "category_display", "size": 100}}
            json_docs["query"]["bool"]["must"][len(json_docs["query"]["bool"]["must"])-1] = {"match": {"category_analyze.suggesting": {"query": string, "operator": "and"}}}
            category_contains = self.es.search(body=json_docs, index=index_name)
            print("\n\t************CATEGORY_CONTAINS BEFORE chosen stuff**********\n", json_docs)
            if searchsuggest:
                if any(self.check_string_in_string(low_cat["key"].lower(), chosen_cat) and low_cat["key"] != "" for low_cat in category_contains["aggregations"]["group_category"]["buckets"]):
                    print("\tCAT CONTAINS category buckets: ", category_contains["aggregations"]["group_category"]["buckets"])
                    if len(chosen_brand) != 0 and chosen_brand[0] != "":
                        for b in chosen_brand:
                            json_docs["query"]["bool"]["must"][7]["bool"]["should"].append({"match": { "brand_display": { "query": "" + b + "", "operator": "and"}}})
                    if len(chosen_cat) != 0 and chosen_cat[0] != "":
                        exclude = False
                        parent_indices = []
                        for c in chosen_cat:
                            for key, value in self.category_name_list.items():
                                if value["name"] == c:
                                    if value["cid"] != 0:
                                        exclude = True
                                    else:
                                        parent_indices.append(chosen_cat.index(c))
                                    break
                        if exclude:
                            for i in parent_indices:
                                chosen_cat.pop(i)
                        parent_id = 0
                        for c in chosen_cat:
                            json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + c + "", "operator": "and"}}})
                            if not exclude:
                                for key, value in self.category_name_list.items():
                                    if value["name"] == c:
                                        parent_id = key
                                        break
                                for key, value in self.category_name_list.items():
                                    if value["cid"] == parent_id:
                                        json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + value["name"] + "", "operator": "and"}}})
                    if len(chosen_size) != 0 and chosen_size[0] != "":
                        for s in chosen_size:
                            for key, value in self.size_list.items():
                                if value["origin_id"] == int(s):
                                    s = value["name"]
                                    break
                            json_docs["query"]["bool"]["must"][6]["bool"]["should"].append({"match": { "sizes": { "query": "" + s + ""}}})
                    if chosen_price_range:
                        if len(chosen_price_range) != 0 and chosen_price_range[0] != "":
                            json_docs["query"]["bool"]["must"][9]["bool"]["should"].append({"range": { "price": { "gte": int(chosen_price_range[0]), "lte": int(chosen_price_range[1])}}})
                    json_docs["aggregations"]["group_brand"] = {"terms": {"field": "brand_display", "size": 100}}
                    print("\n\t************CATEGORY_CONTAINS AFTER chosen stuff**********\n", json_docs)
                    category_contains = self.es.search(body=json_docs, index=index_name)
                    return category_contains
        json_docs["aggregations"]["group_brand"] = {"terms": {"field": "brand_display", "size": 100}}
        json_docs["aggregations"]["group_category"] = {"terms": {"field": "category_display", "size": 100}}
        json_docs["query"]["bool"]["must"][len(json_docs["query"]["bool"]["must"])-1] = {"match": {"name_analyze.suggesting": {"query": string}}}
        print("Before IF (name contain query): ", json_docs)
        if cat_brand_contains:
            if detected_entities[1] != "":
                json_docs["query"]["bool"]["must"][4]["bool"]["should"].pop(0)
            if len(detected_entities[2]) != 0:
                for i in detected_entities[2]:
                    if i != "":
                        json_docs["query"]["bool"]["must"][5]["bool"]["should"].pop(0)
            print("\t\t\t*****must not brand names*****", cat_brand_contains["aggregations"]["group_brand"]["buckets"])
            for brand_name in cat_brand_contains["aggregations"]["group_brand"]["buckets"]:
                json_docs["query"]["bool"]["must_not"].append(                                                                                                                                      {                                                                                                                                                                           "match": {                                                                                                                                                                  "brand_display": {                                                                                                                                                          "query": brand_name["key"],                                                                                                                                               "operator": "and"                                                                                                                                                       }                                                                                                                                                                       }                                                                                                                                                                       })
            print("\t\t\t*****must not cat names*****", cat_brand_contains["aggregations"]["group_category"]["buckets"])
            for cat_name in cat_brand_contains["aggregations"]["group_category"]["buckets"]:
                json_docs["query"]["bool"]["must_not"].append(                                                                                                                                      {                                                                                                                                                                           "match": {                                                                                                                                                                  "category_display": {                                                                                                                                                          "query": cat_name["key"],                                                                                                                                               "operator": "and"                                                                                                                                                       }                                                                                                                                                                       }                                                                                                                                                                       })
        else:
            for brand_name in brand_contains["aggregations"]["group_brand"]["buckets"]:
                json_docs["query"]["bool"]["must_not"].append(
                          {
                            "match": {
                              "brand_display": {
                                "query": brand_name["key"],
                                "operator": "and"
                              }
                            }
                          })
            for category_name in category_contains["aggregations"]["group_category"]["buckets"]:
                json_docs["query"]["bool"]["must_not"].append(
                          {"match":{
                              "category_display": {
                                "query": category_name["key"],
                                "operator": "and"
                              }
                          }
                          })
            print("****took", brand_contains["took"] + category_contains["took"])
        
        name_contains = self.es.search(body=json_docs, index=index_name)
        print("\n\n====\njson docs 3 name contain is (added must_not and pop out all must[7] must[5] etc.): ", json_docs)
        # If run up to this point, clicked suggestion is not either in cat_brand_contains (query of detected entities), category_contains (category match string) or brand_contains (brand match string)
        if searchsuggest:
            if chosen_brand:
                if len(chosen_brand) != 0 and chosen_brand[0] != "":
                    for b in chosen_brand:
                        for key, value in self.brand_name_list.items():
                            if value["alias"] == b:
                                chosen_brand[chosen_brand.index(b)] = value["name"]
                                b = value["name"]
                                break
                        json_docs["query"]["bool"]["must"][7]["bool"]["should"].append({"match": { "brand_display": { "query": "" + b + "", "operator": "and"}}})
            if chosen_cat:
                if len(chosen_cat) != 0 and chosen_cat[0] != "":
                    exclude = False
                    parent_indices = []
                    for c in chosen_cat:
                        for key, value in self.category_name_list.items():
                            if value["name"] == c:
                                if value["cid"] != 0:
                                    exclude = True
                                else:
                                    parent_indices.append(chosen_cat.index(c))
                                    break
                        if exclude:
                            for i in parent_indices:
                                chosen_cat.pop(i)
                        parent_id = 0
                        for c in chosen_cat:
                            json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + c + "", "operator": "and"}}})
                            if not exclude:
                                for key, value in self.category_name_list.items():
                                    if value["name"] == c:
                                        parent_id = key
                                        break
                                for key, value in self.category_name_list.items():
                                    if value["cid"] == parent_id:
                                        json_docs["query"]["bool"]["must"][5]["bool"]["should"].append({"match": { "category_display": { "query": "" + value["name"] + "", "operator": "and"}}})
            if chosen_size:
                if len(chosen_size) != 0 and chosen_size[0] != "":
                    for s in chosen_size:
                        for key, value in self.size_list.items():
                            if value["origin_id"] == int(s):
                                s = value["name"]
                                break
                        json_docs["query"]["bool"]["must"][6]["bool"]["should"].append({"match": { "sizes": { "query": "" + s + ""}}})
            if chosen_price_range:
                if len(chosen_price_range) != 0 and chosen_price_range[0] != "":
                    json_docs["query"]["bool"]["must"][9]["bool"]["should"].append({"range": { "price": { "gte": int(chosen_price_range[0]), "lte": int(chosen_price_range[1])}}})
            name_contains = self.es.search(body=json_docs, index=index_name)
            return name_contains
        if cat_brand_contains:
            for sugg in cat_brand_contains["aggregations"]["group_brand"]["buckets"]:
                name_contains["aggregations"]["group_brand"]["buckets"].append(sugg)
            for sugg in cat_brand_contains["aggregations"]["group_category"]["buckets"]:
                name_contains["aggregations"]["group_category"]["buckets"].append(sugg)
        else: 
            for sugg in brand_contains["aggregations"]["group_brand"]["buckets"]:
                name_contains["aggregations"]["group_brand"]["buckets"].append(sugg)
            for sugg in category_contains["aggregations"]["group_category"]["buckets"]:
                name_contains["aggregations"]["group_category"]["buckets"].append(sugg)
        sorted_brand_bucket = sorted(name_contains["aggregations"]["group_brand"]["buckets"], key = lambda i: i["doc_count"], reverse=True)
        name_contains["aggregations"]["group_brand"]["buckets"] = sorted_brand_bucket
        sorted_cat_bucket = sorted(name_contains["aggregations"]["group_category"]["buckets"], key = lambda i: i["doc_count"], reverse=True)
        name_contains["aggregations"]["group_category"]["buckets"] = sorted_cat_bucket
        results = {
            "categories": [],
            "brands": []
        }
        count = 0
        cat_alias = ""
        brand_alias = ""
        parent_id = 0
        for bucket in name_contains["aggregations"]["group_category"]["buckets"]:
            if count == 20:
                break
            # Uncomment this when alias is available in dictionary_________________________
            for key, value in self.category_name_list.items():
                if value["name"] == bucket["key"]:
                    cid = value["cid"]
                    real_id = key
                    cat_alias = value["alias"]
                    break
            if len(detected_entities[2]) != 0 and detected_entities[2][0] != "":
                flag = 0
                for i in detected_entities[2]:
                    if string.lower() in bucket["key"].lower() or self.no_accent_vietnamese(string.lower()) in self.no_accent_vietnamese(bucket["key"].lower()) or i.lower() in bucket["key"].lower() or self.no_accent_vietnamese(i.lower()) in self.no_accent_vietnamese(bucket["key"].lower()):
                        results["categories"].append({"name": bucket["key"], "is_cat": True, "quantity": bucket["doc_count"], "alias": cat_alias, "parent": cid, "id": real_id})
                        flag = 1
                        break
                if flag == 0:
                    results["categories"].append({"name": bucket["key"], "is_cat": False, "quantity": bucket["doc_count"], "alias": cat_alias, "parent": cid, "id": real_id})
            else:
                if string.lower() in bucket["key"].lower() or self.no_accent_vietnamese(string.lower()) in self.no_accent_vietnamese(bucket["key"].lower()):
                    results["categories"].append({"name": bucket["key"], "is_cat": True, "quantity": bucket["doc_count"], "alias": cat_alias, "parent": cid, "id": real_id})
                else:
                    results["categories"].append({"name": bucket["key"], "is_cat": False, "quantity": bucket["doc_count"], "alias": cat_alias, "parent": cid, "id": real_id})
            count += 1
        count = 0
        for cat in results["categories"]:
           if cat["parent"] == 0:
               for child_cat in results["categories"]:
                   if child_cat["parent"] == cat["id"]:
                       cat["quantity"] += child_cat["quantity"]
        results["categories"] = results["categories"][0:8]
        for bucket in name_contains["aggregations"]["group_brand"]["buckets"]:
            if count == 8:
                break
            # Uncomment this when alias is available in dictionary
            for key, value in self.brand_name_list.items():
                if value["name"] == bucket["key"]:
                    brand_alias = value["alias"]
            if detected_entities[1] != "":
                if string.lower() in bucket["key"].lower() or self.no_accent_vietnamese(string.lower()) in self.no_accent_vietnamese(bucket["key"].lower()) or detected_entities[1].lower() in bucket["key"].lower() or self.no_accent_vietnamese(detected_entities[1].lower()) in self.no_accent_vietnamese(bucket["key"].lower()):
                    results["brands"].append({"name": bucket["key"], "is_brand": True, "quantity": bucket["doc_count"], "alias": brand_alias})
                else:
                    results["brands"].append({"name": bucket["key"], "is_brand": False, "quantity": bucket["doc_count"], "alias": brand_alias})
            else:
                if string.lower() in bucket["key"].lower() or self.no_accent_vietnamese(string.lower()) in self.no_accent_vietnamese(bucket["key"].lower()):
                    results["brands"].append({"name": bucket["key"], "is_brand": True, "quantity": bucket["doc_count"], "alias": brand_alias})
                else:
                    results["brands"].append({"name": bucket["key"], "is_brand": False, "quantity": bucket["doc_count"], "alias": brand_alias})
            count += 1
        if not searchsuggest:
            return results

    def no_accent_vietnamese(self, s):
        s = s.lower()
        s = re.sub('[áàảãạăắằẳẵặâấầẩẫậ]', 'a', s)
        s = re.sub('[éèẻẽẹêếềểễệ]', 'e', s)
        s = re.sub('[óòỏõọôốồổỗộơớờởỡợ]', 'o', s)
        s = re.sub('[íìỉĩị]', 'i', s)
        s = re.sub('[úùủũụưứừửữự]', 'u', s)
        s = re.sub('[ýỳỷỹỵ]', 'y', s)
        s = re.sub('đ', 'd', s)
        return s

    def return_hot_keys(self):
        right_now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        query = {"from": 0, "size": 0,
            "query": {
                "range": {
                    "created_at": {
                        "gte": "now-15d/d",
                        "lt": "now/d"
                    }
                }
            },
            "aggregations": {
               "group_searches": {
                   "terms": {
                       "field": "search_string",
                       "size": 10
                   }
               }
            }
        }
        hot_key_results = self.es.search(doc_type="log_action", index="log_action_index", body=query)
        return hot_key_results["aggregations"]["group_searches"]["buckets"]
