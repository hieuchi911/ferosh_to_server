# -*- coding: utf-8 -*-
import pymysql.cursors
import MySQLdb
import json
from sys import platform
import pathlib


class DictionaryHandler():
    def __init__(self):
        self.category_dict = []
        self.brand_dict = []
        self.color_dict = []
        self.specialCharacters = ".,=+-_!;/()*\"&^:#|\n\t'"
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.PATH_DIR = "\\"
        if platform == "linux" or platform == "linux2":
            self.PATH_DIR = "/"
        self.path = str(pathlib.Path(__file__).parent.absolute())
        with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "test_color.json", encoding="utf8") as f:
            self.color_dict = json.loads(f.read())

        with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "test_brand.json", encoding="utf8") as f:
            self.brand_dict = json.loads(f.read())

        with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "test_cat.json", encoding="utf8") as f:
            self.category_dict = json.loads(f.read())

    def mysqConnect(self):
        self.conn = pymysql.connect(user="root",
                                    passwd="muathuhanoi2014",
                                    db="ferosh",
                                    host="127.0.0.1",
                                    charset="utf8",
                                    cursorclass=pymysql.cursors.DictCursor)
        self.cursor = self.conn.cursor()

    def mysqlQuery(self, sql, params=()):
        try:
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
            self.conn.commit()
        except (AttributeError, MySQLdb.OperationalError) as e:
            print('exception generated during sql connection: ', e)
            self.mysqConnect()
            cursor = self.conn.cursor()
            cursor.execute(sql, params)
        return cursor

    def display(self, page, limit, col_type, col_name, status=None):
        offset = (page - 1) * limit
        sql = ""
        results = []
        if not col_type:
            sql = """
                SELECT * FROM (
                    (
                        (
                        SELECT 'cat' AS type, t_2.id, t_2.name, t_1.total_count, ifnull(t_1.alternatives, "") as alternatives, t_2.status
                        FROM
                            (
                            SELECT alias, GROUP_CONCAT(NAME) AS alternatives, COUNT(*) AS total_count
                            FROM tbl_types_search
                            WHERE sub = 1
                            GROUP BY alias
                            ) t_1, tbl_types_search t_2
                        WHERE t_1.alias = t_2.alias AND t_2.sub = 0
                        )
                UNION                                                                                                                                                                       (                                                                                                                                                                           SELECT 'cat' AS type, t_2.id, t_2.name, 0 as total_count, "" AS alternatives, t_2.status                                                                                FROM tbl_types_search t_2                                                                                                                                               WHERE t_2.alias NOT IN (                                                                                                                                                SELECT alias                                                                                                                                                            FROM tbl_types_search                                                                                                                                               WHERE sub = 1                                                                                                                                                           GROUP BY alias                                                                                                                                                          )                                                                                                                                                                       )
                UNION
                    (
                    SELECT
                        'brand' AS type, t_2.id, t_2.name, t_1.total_count, ifnull(t_1.alternatives, "") as alternatives, t_2.status
                    FROM
                        (
                        SELECT alias, GROUP_CONCAT(NAME) AS alternatives, COUNT(*) AS total_count
                        FROM tbl_brands_search
                        WHERE sub = 1
                        GROUP BY alias
                        ) t_1, tbl_brands_search t_2
                    WHERE t_1.alias = t_2.alias AND t_2.sub = 0
                )
                UNION
                    (
                        SELECT 'brand' AS type, t_2.id, t_2.name, 0 as total_count, "" AS alternatives, t_2.status
                        FROM tbl_brands_search t_2
                        WHERE t_2.alias NOT IN (
                        SELECT alias
                        FROM tbl_brands_search
                        WHERE sub = 1
                        GROUP BY alias
                                  )
                              )
                UNION
                    (
                    SELECT
                        'color' AS type, t_2.id, t_2.name, t_1.total_count, ifnull(t_1.alternatives, "") as alternatives, t_2.status
                    FROM
                        (
                        SELECT cid, GROUP_CONCAT(NAME) AS alternatives, COUNT(*) AS total_count
                        FROM tbl_colors
                        WHERE sub = 1 AND cid != 0
                        GROUP BY cid
                    ) t_1, tbl_colors t_2
                	WHERE t_1.cid = t_2.id
                )
                    ) AS main_table)
                """
        else:
            if col_type == "type":
                sql = f"""SELECT * FROM     
                              (
                                  (                                                           
                                      SELECT 'cat' AS type, t_2.id, t_2.name, t_1.total_count, IFNULL(t_1.alternatives, "") AS alternatives, t_2.status      
                                      FROM 
                                      (                                                                                                                                                                           SELECT alias, GROUP_CONCAT(NAME) AS alternatives, COUNT(*) AS total_count                                                                                               FROM tbl_types_search                                                                                                                                                   WHERE sub = 1
                                          GROUP BY alias                                                                                                                                                      ) t_1, tbl_types_search t_2                                                                                                                                             WHERE t_1.alias = t_2.alias AND t_2.sub = 0                                                                                                                         )
                                  UNION                                                                                                                                                                   (                                                                                                                                                                           SELECT 'cat' AS type, t_2.id, t_2.name, 0 as total_count, "" AS alternatives, t_2.status                                                                                FROM tbl_types_search t_2                                                                                                                                               WHERE t_2.alias NOT IN (                                                                                                                                                SELECT alias                                                                                                                                                            FROM tbl_types_search
                                  WHERE sub = 1                                                                                                                                                           GROUP BY alias                                                                                                                                                          )                                                                                                                                                                   )) as main_table"""
                if col_name:
                    sql += " WHERE LOWER(main_table.alternatives) LIKE LOWER(%s) or LOWER(main_table.name) LIKE LOWER(%s)"
            if col_type == "brand":
                sql = f"""SELECT * FROM
                          (
                              (
                                  SELECT 'brand' AS type, t_2.id, t_2.name, t_1.total_count, IFNULL(t_1.alternatives, "") AS alternatives, t_2.status
                                  FROM
                                  (
                                      SELECT alias, GROUP_CONCAT(NAME) AS alternatives, COUNT(*) AS total_count
                                      FROM tbl_brands_search
                                      WHERE sub = 1
                                      GROUP BY alias
                                  ) t_1, tbl_brands_search t_2
                                  WHERE t_1.alias = t_2.alias AND t_2.sub = 0
                              )
                              UNION
                              (
                                  SELECT 'brand' AS type, t_2.id, t_2.name, 0 as total_count, "" AS alternatives, t_2.status
                                  FROM tbl_brands_search t_2
                                  WHERE t_2.alias NOT IN (
                                  SELECT alias
                                  FROM tbl_brands_search
                                  WHERE sub = 1
                                  GROUP BY alias
                                  )
                              )) as main_table"""
                if col_name:
                    sql += " WHERE LOWER(main_table.alternatives) LIKE LOWER(%s) or LOWER(main_table.name) LIKE LOWER(%s)"
            if col_type == "color":
                sql = f"""SELECT * FROM                                                                                                                                                           (                                                                                                                                                                           (                                                                                                                                                                           SELECT 'color' AS type, t_2.id, t_2.name, t_1.total_count, IFNULL(t_1.alternatives, "") AS alternatives, t_2.status                                                     FROM                                                                                                                                                                    (                                                                                                                                                                           SELECT alias, GROUP_CONCAT(NAME) AS alternatives, COUNT(*) AS total_count                                                                                               FROM tbl_colors                                                                                                                                                         WHERE sub = 1
                                      GROUP BY alias                                                                                                                                                      ) t_1, tbl_colors t_2                                                                                                                                                   WHERE t_1.alias = t_2.alias AND t_2.sub = 0                                                                                                                         )
                              UNION                                                                                                                                                                   (                                                                                                                                                                           SELECT 'color' AS type, t_2.id, t_2.name, 0 as total_count, "" AS alternatives, t_2.status                                                                              FROM tbl_colors t_2                                                                                                                                                     WHERE t_2.cid NOT IN (                                                                                                                                                  SELECT cid                                                                                                                                                              FROM tbl_colors
                                  WHERE sub = 1                                                                                                                                                           GROUP BY cid                                                                                                                                                          )                                                                                                                                                                   )) as main_table"""
                if col_name:
                    sql += " WHERE LOWER(main_table.alternatives) LIKE LOWER(%s) or LOWER(main_table.name) LIKE LOWER(%s)"
            total_count = 0
            if page or limit:
                print(f"offset {offset} and limit {limit}")
                if status:
                    sql += f" and main_table.status='{status}'"
                if col_name:
                    param = '%{}%'.format(col_name)
                    print(param)
                    curr = self.mysqlQuery(sql, (param, param))
                else:
                    curr = self.mysqlQuery(sql)
                total_count = len(curr.fetchall())
                sql += "\nORDER BY main_table.name ASC\nLIMIT %s,%s"
                if col_name:
                    param = '%{}%'.format(col_name)
                    curr = self.mysqlQuery(sql, (param, param, offset, limit))
                    print(sql)
                else:
                    curr = self.mysqlQuery(sql, (offset, limit))
                #print("RESULTS: ", sql)
            if curr:
                results = curr.fetchall()
                print("IN HERE", results)
            else:
                results = []
            output = {}
            for i in results:
                if i["name"] not in output.keys():
                    output[i["name"]] = {
                        "type": i["type"],
                        "id": i["id"],
                        "status": i["status"],
                        "alternatives": i["alternatives"]
                    }
            returning_output = {"total": total_count, "result": output}
            return returning_output
        if status:
            sql += f"WHERE main_table.status='{status}'"
        if col_name:
            if status:
                sql += """ and LOWER(main_table.alternatives) LIKE LOWER(%s) or LOWER(main_table.name) LIKE LOWER(%s)
                           ORDER BY `main_table`.`total_count` DESC"""
            else:
                sql += """WHERE LOWER(main_table.alternatives) LIKE LOWER(%s) or LOWER(main_table.name) LIKE LOWER(%s)
                        ORDER BY `main_table`.`total_count` DESC"""
            param = '%{}%'.format(col_name)
            curr = self.mysqlQuery(sql, (param, param))
            total_count = len(curr.fetchall())
            sql += "\nLIMIT %s, %s"
            curr = self.mysqlQuery(sql, (param, param, offset, limit))
        else:
            sql += "ORDER BY `main_table`.`total_count` DESC"
            curr = self.mysqlQuery(sql)
            total_count = len(curr.fetchall())
            sql += "\nLIMIT %s, %s"
            curr = self.mysqlQuery(sql, (offset, limit))
        if curr:
            results = curr.fetchall()
        print(sql)
        print(results)
        output = {}
        for i in results:
            if i["name"] not in output.keys():
                output[i["name"]] = {
                    "type": i["type"],
                    "id": i["id"],
                    "status": i["status"],
                    "alternatives": i["alternatives"]
                }
        returning_output = {"total": total_count, "result": output}
        return returning_output

    def update_dict_files(self, dict_type):
        if dict_type == "cat":
            sql = f"""SELECT * FROM tbl_types_search"""
            curr = self.mysqlQuery(sql)
            all_cats = curr.fetchall()
            with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "tbl_types.json", "w", encoding="utf8") as f:
                json.dump(all_cats, f, indent=4)
                f.close()
        elif dict_type == "brand":
            sql = f"""SELECT * FROM tbl_brands_search"""
            curr = self.mysqlQuery(sql)
            all_brands = curr.fetchall()
            with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "tbl_brands.json", "w", encoding="utf8") as f:
                json.dump(all_brands, f, indent=4)
                f.close()
        elif dict_type == "color":
            sql = f"""SELECT * FROM tbl_colors"""
            curr = self.mysqlQuery(sql)
            all_colors = curr.fetchall()
            with open(self.path + self.PATH_DIR + "dictionaries" + self.PATH_DIR + "tbl_colors.json", "w", encoding="utf8") as f:
                json.dump(all_colors, f, indent=4)
                f.close()

    def update_alt(self, main_type, main_id, alt_left, search):
        alts = alt_left.split(",")
        if main_type == "cat":
            sql = f"""DELETE FROM tbl_types_search WHERE tbl_types_search.sub = 1 and tbl_types_search.cid = {main_id}"""
            #sql = f"""DELETE FROM cat_test WHERE cat_test.sub = 1 and cat_test.cid = {main_id}"""
            curr = self.mysqlQuery(sql)
            for a in alts:
                if a != "":
                    self.add_alt(main_type, main_id, a)
            self.update_dict_files("cat")
            if curr:
                search.update_dictionaries()
                return curr.fetchall()
            else:
                return []
        elif main_type == "brand":
            sql = f"""DELETE FROM tbl_brands_search WHERE tbl_brands_search.sub = 1 and tbl_brands_search.cid = {main_id}"""
            #sql = f"""DELETE FROM brand_test WHERE brand_test.sub = 1 and brand_test.cid = {main_id}"""
            curr = self.mysqlQuery(sql)
            for a in alts:
                if a != "":
                    self.add_alt(main_type, main_id, a)
            self.update_dict_files("brand")
            if curr:
                search.update_dictionaries() 
                return curr.fetchall()
            else:
                return []
        elif main_type == "color":
            sql = f"""DELETE FROM tbl_colors WHERE tbl_colors.sub = 1 and tbl_colors.cid = {main_id}"""
            #sql = f"""DELETE FROM color_test WHERE color_test.sub = 1 and color_test.cid = {main_id}"""
            curr = self.mysqlQuery(sql)
            for a in alts:
                if a != "":
                    self.add_alt(main_type, main_id, a)
            self.update_dict_files("color")
            if curr:
                search.update_dictionaries() 
                return curr.fetchall()
            else:
                return []

    def add_alt(self, main_type, main_id, alt_name):  # id should be incremented automatically
        if main_type == "cat":
            sql1 = f"""SELECT * FROM tbl_types_search WHERE id = {main_id}"""  # extract cid, sid, origin_id, alias, status from the main name
            #sql1 = f"""SELECT * FROM cat_test WHERE id = {main_id}"""
            curr = self.mysqlQuery(sql1)
            main_cat = curr.fetchall()
            sql = f"""
                    INSERT INTO tbl_types_search (cid, sid, pid, origin_id, alias, name, status, sub)
                    VALUES ("{main_id}", "{main_cat[0]['cid']}", "{main_cat[0]['sid']}",
                            "{main_cat[0]['origin_id']}", "{main_cat[0]['alias']}",
                            "{alt_name}", "{main_cat[0]['status']}", 1)
                    """
            curr = self.mysqlQuery(sql)
        if main_type == "brand":
            sql1 = f"""SELECT * FROM tbl_brands_search WHERE id = {main_id}"""  # extract origin_id, alias, status from the main name
            #sql1 = f"""SELECT * FROM brand_test WHERE id = {main_id}"""
            curr = self.mysqlQuery(sql1)
            main_brand = curr.fetchall()
            sql = f"""INSERT INTO tbl_brands_search (cid, origin_id, alias, name, status, sub)
                    VALUES ("{main_id}", "{main_brand[0]['origin_id']}", "{main_brand[0]['alias']}",
                            "{alt_name}", "{main_brand[0]['status']}", 1)
                    """
            curr = self.mysqlQuery(sql)
        if main_type == "color":
            sql1 = f"""SELECT * FROM tbl_colors WHERE id = {main_id}"""  # extract origin_id, alias, status from the main name
            #sql1 = f"""SELECT * FROM color_test WHERE id = {main_id}"""
            curr = self.mysqlQuery(sql1)
            main_color = curr.fetchall()
            sql = f"""INSERT INTO tbl_colors (cid, name, status, sub)
                    VALUES ({main_id}, "{alt_name}", "{main_color[0]['status']}", 1)
                    """
            curr = self.mysqlQuery(sql)

    # id should be incremented automatically, if main_name already exist -> flip status from N to Y
    def add_new_name(self, main_type, main_name, main_alias=None, main_cid=None, main_origin_id=None, main_status=None, search=None):
        if main_type == "cat":
            return 0
            cid = 0
            sid = 0
            pid = 0
            if main_cid != 0:  # if main cid = 0, this is a parent cat, otherwise child cat
                sql1 = f"""SELECT * FROM cat_test WHERE id = {main_cid}"""  # extract cid, sid, origin_id, alias, status from the main name
                #sql1 = f"""SELECT * FROM cat_test WHERE id = {main_cid}"""
                curr = self.mysqlQuery(sql1)
                parent_cat = curr.fetchall()
                cid = main_cid
                sid = parent_cat[0]['cid']
                pid = parent_cat[0]['sid']
            sql1 = f"""SELECT * FROM cat_test where origin_id={main_origin_id} and sub=0"""
            the_origin_id = self.mysqlQuery(sql1).fetchall()
            print("the origin id: ", the_origin_id)
            if len(the_origin_id) == 1:
                sql = f"""UPDATE cat_test SET cid={cid}, sid={sid}, pid={pid},
                                                      status="{main_status}", alias="{main_alias}",
                                                      name="{main_name}"
                          WHERE origin_id={main_origin_id} AND sub=0"""
            else:
                sql = f"""INSERT INTO cat_test (cid, sid, pid, origin_id, alias, name, status, sub)
                          VALUES ({cid}, {sid}, {pid}, {main_origin_id}, "{main_alias}", "{main_name}", "{main_status}", 0);"""
            curr = self.mysqlQuery(sql)
            if len(the_origin_id) == 1:
                sql = f"""UPDATE cat_test SET cid={cid}, sid={sid}, pid={pid}, status="{main_status}", alias="{main_alias}"
                          WHERE origin_id={main_origin_id} AND sub=1"""
                curr = self.mysqlQuery(sql)
            self.update_dict_files("cat")
            if curr:
                search.update_dictionaries()
                return curr.fetchall()
            else:
                return []
        if main_type == "brand":
            sql1 = f"""SELECT * FROM tbl_brands_search where origin_id={main_origin_id} and sub=0"""
            the_origin_id = self.mysqlQuery(sql1).fetchall()
            if len(the_origin_id) == 1:
                sql = f"""UPDATE tbl_brands_search SET status="{main_status}", alias="{main_alias}", name="{main_name}"
                          WHERE origin_id={main_origin_id} AND sub=0"""
            else:
                sql = f"""INSERT INTO tbl_brands_search (cid, origin_id, alias, name, status, sub)
                          VALUES (0, {main_origin_id}, "{main_alias}", "{main_name}", "{main_status}", 0);"""
            curr = self.mysqlQuery(sql)
            if len(the_origin_id) == 1:
                sql = f"""UPDATE tbl_brands_search SET status="{main_status}", alias="{main_alias}"
                          WHERE origin_id={main_origin_id} AND sub=1"""
                curr = self.mysqlQuery(sql)
            self.update_dict_files("brand")
            if curr:
                search.update_dictionaries()
                return curr.fetchall()
            else:
                return []
        if main_type == "color":
            sql = f"""INSERT INTO tbl_colors (cid, name, status, sub)
                    VALUES (0, "{main_name}", "{main_status}", 0)
                    ON DUPLICATE KEY UPDATE id=LAST_INSERT_ID(id), status=VALUES(status)"""
            curr = self.mysqlQuery(sql)
            edited_color_id = curr.lastrowid
            sql1 = f"""SELECT max(id) AS max_id FROM tbl_colors"""
            max_id = self.mysqlQuery(sql1).fetchall()
            if edited_color_id <= max_id[0]['max_id']:
                sql = f"""UPDATE tbl_colors SET status = "{main_status}" WHERE cid = {edited_color_id} AND sub = 1"""
                curr = self.mysqlQuery(sql)
            self.update_dict_files("color")
            if curr:
                search.update_dictionaries() 
                return curr.fetchall()
            else:
                return []
