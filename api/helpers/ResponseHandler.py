from flask import request, jsonify, make_response
import json


class Response():
    def data(self, msg, data):
        output = {
            "success": True,
            "msg": msg,
            "data": data
        }
        return make_response(jsonify(output), 200)

    def message(self, stt, msg):
        success = True
        if stt != 200:
            success = False
        output = {
            "success": success,
            "msg": msg
        }

        return make_response(jsonify(output), stt)

    def error(self, stt, msg, err=""):
        output = {
            "success": False,
            "msg": msg,
            "err": err
        }

        return make_response(jsonify(output), stt)


