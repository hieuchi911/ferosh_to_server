from marshmallow import Schema, fields, validates, ValidationError
from marshmallow.validate import Length, Range

# from api.models.CustomerModel import Customers
# from api.models.PetModel import Pets
# from api.models.ProductModel import Product
# from api.models.ConfigModel import Examination_reason, Departments, Companies

# no longer than 60 chars
title = fields.Str(required=True, validate=Length(max=60))
# no longer than 1000 chars
note = fields.Str(required=True, validate=Length(max=1000))
# at least 1
user_id = fields.Int(required=True, validate=Range(min=1))
time_created = fields.DateTime(required=True)

class createMedicalRecord(Schema):
    customer_id = fields.Int(required=True)
    pet_id = fields.Int(required=True)
    examination_date = fields.DateTime(required=True)
    reason_id = fields.Int(rerequired=True)
    department_id = fields.Int(rerequired=True)
    company_id = fields.Int(rerequired=True)
    product_id = fields.Int(rerequired=True)

    @validates('customer_id')
    def customerExists(self, value):
        customer = Customers.query.filter_by(id=value).first()

        if not customer:
            raise ValidationError("customer not found")

    @validates('pet_id')
    def petExists(self, value):
        pet = Pets.query.filter_by(id=value).first()

        if not pet:
            raise ValidationError("pet not found")

    @validates('reason_id')
    def reasontExists(self, value):
        reason = Examination_reason.query.filter_by(id=value).first()

        if not reason:
            raise ValidationError("reason not found")

    @validates('department_id')
    def departmentExists(self, value):
        department = Departments.query.filter_by(id=value).first()

        if not department:
            raise ValidationError("department not found")

    @validates('company_id')
    def companyExists(self, value):
        company = Companies.query.filter_by(id=value).first()

        if not company:
            raise ValidationError("company not found")

    @validates('product_id')
    def productExists(self, value):
        product = Product.query.filter_by(id=value).first()

        if not product:
            raise ValidationError("product not found")
