# -*- coding: utf-8 -*-
from elasticsearch import Elasticsearch
import io
import csv

class ActionLogger():
    def __init__(self):
        self.specialCharacters = ".,=+-_!;/()*\"&^:#|\n\t'"
        self.DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"
        self.es = Elasticsearch('localhost:9200')

    def searching(self, page, limit, index_name, from_date_filter=None, to_date_filter=None, grouping=None, group_limit=200, sort_type=None, sort_col=None):
        offset = (page - 1) * limit
        json_docs = {
            "sort": {
                "created_at": {
                    "order": "desc"
                }
            },
            "from": offset, "size": limit,
            "query": {
                "bool": {
                    "must": [
                        #{"range": {"created_at": {"gt": "now-31d/d"}}}
                    ]
                }
            },
            "aggregations": {
                "group_terms": {"terms": {"field": "search_string", "size": group_limit},
                "aggs": {
                    "click_count": {
                        "sum": {
                            "field": "total_clicks"
                        }
                    },
                    "view_count": {
                        "sum": {
                            "field": "total_views"
                        }
                    },
                    "detected_cat": {
                        "terms": {
                            "script": "_source.detected.category"
                        }
                    },
                    "detected_brand": {
                        "terms": {
                            "script": "_source.detected.brand"
                        }
                    },
                    "detected_color": {
                        "terms": {
                        "script": "_source.detected.color"
                        }
                    }
                }}
            }
        }
        if grouping:
            json_docs["query"]["bool"]["must"].append({"range": {"created_at": {"gt": "now-31d/d"}}})
            if from_date_filter and to_date_filter:
                json_docs["query"]["bool"]["must"].pop()
                json_docs["query"]["bool"]["must"].append({"range": {"created_at": {"gte": from_date_filter}}})
                json_docs["query"]["bool"]["must"].append({"range": {"created_at": {"lte": to_date_filter}}})
            if sort_col:
                if sort_col == "view":
                    if sort_type == "asc":
                        json_docs["aggregations"]["group_terms"]["terms"]["order"] = { "view_count": "asc" }
                    else:
                        json_docs["aggregations"]["group_terms"]["terms"]["order"] = { "view_count": "desc" }
                elif sort_col == "click":
                    if sort_type == "asc":
                        json_docs["aggregations"]["group_terms"]["terms"]["order"] = { "click_count": "asc" }
                    else:
                        json_docs["aggregations"]["group_terms"]["terms"]["order"] = { "click_count": "desc" }
        else:
            if from_date_filter:
                json_docs["query"]["bool"]["must"].append({"range": {"created_at": {"gte": from_date_filter}}})
            if to_date_filter:
                json_docs["query"]["bool"]["must"].append({"range": {"created_at": {"lte": to_date_filter}}})
            if sort_col:
                json_docs["sort"].pop("created_at")
                if sort_col == "view":
                    if sort_type == "asc":
                        json_docs["sort"]["total_views"] = {"order": "asc"}
                    else:
                        json_docs["sort"]["total_views"] = {"order": "desc"}
                elif sort_col == "click":
                    if sort_type == "asc":
                        json_docs["sort"]["total_clicks"] = {"order": "asc"}
                    else:
                        json_docs["sort"]["total_clicks"] = {"order": "desc"}
        print("\n====================\njson_docs\n====================\n", json_docs)
        results = self.es.search(body=json_docs, index=index_name)
        print("\n====================\nresults\n====================\n")#, len(results['aggregations']['group_terms']['buckets']))
        return results

    def return_searches(self, searches, grouping=None):
        returning_searches = {}
        print("\n====================\nSEARCHES\n====================\n")
        if grouping:
            #print("query time taken: ", searches["took"])
            for bucket in searches["aggregations"]["group_terms"]["buckets"]:
                if 1 > 0:# bucket['key'].lower() not in returning_searches.keys():
                    cat = ""
                    color = ""
                    brand = ""
                    for i in bucket['detected_cat']['buckets']:
                        cat += i["key"] + ", "
                    for i in bucket['detected_color']['buckets']:
                        color += i["key"] + ", "
                    for i in bucket['detected_brand']['buckets']:
                        brand += i["key"] + ", "
                    returning_searches[bucket['key']] = {
                        "search_string": bucket['key'],
                        "log_view": bucket['view_count']['value'],
                        "log_click": bucket['click_count']['value'],
                        "detected_cat": cat[:-2],
                        "detected_brand": brand[:-2],
                        "detected_color": color[:-2],
                        "quantity": bucket['doc_count']
                        }
                # else:
                #     returning_searches[bucket['key'].lower()]["log_view"] += bucket['view_count']['value']
                #     returning_searches[bucket['key'].lower()]["log_click"] += bucket['click_count']['value']
                #     returning_searches[bucket['key'].lower()]["quantity"] += bucket['doc_count']
            output = {"total": len(searches["aggregations"]["group_terms"]["buckets"]), "time": searches["took"], "searches": returning_searches}
            #print("total time: ", time.time() - start_ + searches["took"])
        else:
            for res in searches["hits"]["hits"]:
                cat = res['_source']['detected']["category"]
                color = res['_source']['detected']["color"]
                brand = res['_source']['detected']["brand"]
                if str(type(res['_source']['detected']["category"])) == "<class 'list'>":
                    cat = ", ".join(res['_source']['detected']["category"])
                if str(type(res['_source']['detected']["brand"])) == "<class 'list'>":
                    brand = ", ".join(res['_source']['detected']["brand"])
                if str(type(res['_source']['detected']["color"])) == "<class 'list'>":
                    color = ", ".join(res['_source']['detected']["color"])
                returning_searches[res['_source']["search_id"]] = {
                    "search_string": res['_source']["search_string"],
                    "log_view": res['_source']["total_views"],
                    "log_click": res['_source']["total_clicks"],
                    "created_at": res['_source']["created_at"],
                    "detected_cat": cat,
                    "detected_brand": brand,
                    "detected_color": color
                    }
            output = {"total": searches["hits"]["total"], "time": searches["took"], "searches": returning_searches}
        print("\n====================\nReturning_searches\n====================\n", len(returning_searches))
        return output

    def export_searches(self, searches, grouping=None):
        data = []
        output = io.StringIO()
        writer = csv.writer(output, delimiter=',')
        if grouping:
            for search in searches:
                data.append([searches[search]["search_string"], searches[search]["log_view"],
                             searches[search]["log_click"], searches[search]["detected_cat"],
                             searches[search]["detected_brand"], searches[search]["detected_color"],
                             searches[search]["quantity"]])
            writer.writerows(data)
            your_csv_string = output.getvalue()
        else:
            for search in searches:
                data.append([searches[search]["search_string"], searches[search]["log_view"],
                             searches[search]["log_click"], searches[search]["detected_cat"],
                             searches[search]["detected_brand"], searches[search]["detected_color"],
                             searches[search]["created_at"]])
            writer.writerows(data)
            your_csv_string = output.getvalue()
        return output

