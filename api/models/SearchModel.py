# -*- coding: utf-8 -*-

from api import db

class Search(db.Model):
    search_id_test = db.Column(db.Integer, primary_key=True)
    search_token = db.Column(db.String(50))
