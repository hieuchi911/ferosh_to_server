# -*- coding: utf-8 -*-
from flask import request
from flask_restful import Resource
from api.helpers.ResponseHandler import Response
from api.helpers.DictionariesHandler import DictionaryHandler
from api.controllers.SearchController import *

dict_handler = DictionaryHandler()
#print("AT DICTIONARY CONTROLLER LEVEL\nsearch cat name list at index 206: \n", search.brand_name_list)
class DictDisplayer(Resource):
    def get(self):
        response = Response()
        page = request.args.get('page', default=1, type=int)
        limit = request.args.get('limit', default=200, type=int)
        col_type = request.args.get('type', default="", type=str)
        col_name = request.args.get('name', default="", type=str)
        status = request.args.get('status', default="", type=str)
        print("page: ", page)
        print("limit: ", limit)
        print("col name: ", col_name)
        print("col type: ", col_type)
        results = dict_handler.display(page, limit, col_type, col_name, status=status)

        return response.data("Display dictionaries", results)

class UpdateAlternative(Resource):
    def post(self):
        response = Response()

        main_type = request.form.get('type', default="", type=str)
        main_id = request.form.get('main_id', default=-1, type=int)
        alt_left = request.form.get('alt_name', default=-1, type=str)

        out = dict_handler.update_alt(main_type, main_id, alt_left, search)
        return response.data(f"Removed alternative", out)

class AddKeyword(Resource):
    def post(self):
        response = Response()

        main_type = request.form.get('type', default="", type=str)
        main_name = request.form.get('main_name', default="", type=str)
        origin_id = request.form.get('origin_id', default=-1, type=int)
        main_cid = request.form.get('main_cid', default=-1, type=int)
        alias = request.form.get('alias', default="", type=str)
        main_status = request.form.get('status', default="", type=str)

        out = dict_handler.add_new_name(main_type, main_name, main_alias=alias, main_cid=main_cid, main_status=main_status, main_origin_id=origin_id, search=search)  # id should be incremented automatically
        return response.data(f"Added keyword", out)
