from flask import request, jsonify
from flask_restful import Resource
# from werkzeug.security import generate_password_hash
import uuid
from api.helpers.ResponseHandler import Response

from api import db
# from api.models.UserModel import User, Message, Session
from api.middlewares.tokenize import tokenize
import math
# from api.repositories.ConfigRepository import MessageRepo, SessionRepo
import base64
import time

class UserSession(Resource):
    method_decorators = [tokenize]
    def get(self, current_user):
        response = Response()
        page = request.args.get('page', default=1, type=int)
        limit = request.args.get('limit', default=15, type=int)
        offset = (page - 1) * limit
        #
        # sessions = Session.query
        # count = sessions.count()
        # sessions = sessions.order_by(Session.id.desc()).limit(limit).offset(offset).all()
        # totalPage = math.ceil(count / limit)
        #
        # result = []
        # for session in sessions:
        #     repo = SessionRepo()
        #     res = SessionRepo.repository(repo, session)
        #     result.append(res)
        #
        # output = {
        #     "total_page": totalPage,
        #     "total": count,
        #     "page": page,
        #     "limit": limit,
        #     "result": result,
        # }
        # return response.data("Get all sessions", output)

class UserAll(Resource):
    method_decorators = [tokenize]
    def get(self, current_user):
        response = Response()
        page = request.args.get('page', default=1, type=int)
        limit = request.args.get('limit', default=15, type=int)
        session_id = request.args.get('session_id', default = '', type = str)

        print ("====")
        print (session_id)
        print ("====")

        offset = (page - 1) * limit

        # messages = Message.query.filter_by(session_id=session_id)
        # count = messages.count()
        # messages = messages.order_by(Message.id.desc()).limit(limit).offset(offset).all()
        # totalPage = math.ceil(count / limit)
        #
        # result = []
        # for message in messages:
        #     repo = MessageRepo()
        #     res = MessageRepo.repository(repo, message)
        #     result.append(res)
        #
        # output = {
        #     "total_page": totalPage,
        #     "total": count,
        #     "page": page,
        #     "limit": limit,
        #     "result": result,
        # }
        # return response.data("Get all images by session id", output)

    def post(self, current_user):
        response = Response()
        message = request.form.get('message', default = '', type = str)
        image = request.form.get('image', default = '', type = str)

        # GET BASE64 CONVERT TO IMAGE
        imgdata = base64.b64decode(image)
        fileName = 'image2txt_'+str(int(time.time()))+'.jpg'
        filePath = '/'
        try:
            with open(filePath + fileName, 'wb') as f:
                f.write(imgdata)
        except Exception as f:
            print ("=== ERROR CREATE IMAGE ===")

        session_id = request.form.get('session_id', default = '', type = str)
        order_by = request.form.get('order_by', default = '', type = int)
        if not current_user.admin:
            return jsonify({'message': 'Cannot perform that function!'})

        # new_message = Message(message=message, image=image, image_url=fileName, session_id=session_id, order_by=order_by, status=0)
        #
        # # check session
        # session = Session.query.filter_by(session=session_id).first()
        # if not session:
        #     new_session = Session(session=session_id)
        #     db.session.add(new_session)
        #
        # db.session.add(new_message)
        # db.session.commit()

        return response.data("Create message successful", "true")