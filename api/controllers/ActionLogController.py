# -*- coding: utf-8 -*-
from flask import request, make_response
from flask_restful import Resource
from api.helpers.ResponseHandler import Response
from api.helpers.ActionLogHandler import ActionLogger

logger = ActionLogger()

class LogDisplayer(Resource):
    def post(self):
        response = Response()

        page = request.form.get('page', default=1, type=int)
        limit = request.form.get('limit', default=200, type=int)
        from_date = request.form.get('from', default="", type=str)
        to_date = request.form.get('to', default="", type=str)
        grouping = request.form.get('group', default="", type=str)
        sort_type = request.form.get('sort_type', default="", type=str)
        sort_col = request.form.get('sort_col', default="", type=str)

        results = logger.searching(page, limit, "log_action_index", from_date_filter=from_date, to_date_filter=to_date, grouping=grouping, group_limit=limit, sort_type=sort_type, sort_col=sort_col)
        outputs = logger.return_searches(results, grouping)
        return response.data(f"Display following log", outputs)

class LogExporter(Resource):
    def post(self):
        response = Response()

        page = request.form.get('page', default=1, type=int)
        limit = request.form.get('limit', default=200, type=int)
        from_date = request.form.get('from', default="", type=str)
        to_date = request.form.get('to', default="", type=str)
        grouping = request.form.get('group', default="", type=str)

        results = logger.searching(page, limit, "log_action_index", from_date_filter=from_date, to_date_filter=to_date, grouping=grouping, group_limit=limit)
        outputs = logger.return_searches(results, grouping)
        export = logger.export_searches(outputs["searches"], grouping)

        output = make_response(export.getvalue())
        output.headers["Content-Disposition"] = "attachment; filename=export.csv"
        output.headers["Content-type"] = "text/csv"
        return output 
