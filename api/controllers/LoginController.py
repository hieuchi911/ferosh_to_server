# -*- coding: utf-8 -*-
from flask import request, jsonify
from flask_restful import Resource
from api.helpers.ResponseHandler import Response
import datetime
import jwt
import hashlib

from api.middlewares.tokenize import tokenize
from api import app
from api.models.UserModel import User

class Login(Resource):
    def post(self):
        response = Response()
        username = request.form.get('username', default = '', type = str)
        password = request.form.get('password', default = '', type = str)

        user = User.query.filter_by(name=username).filter_by(password=hashlib.sha256(password.encode('utf-8')).hexdigest()).first()
        if user:
            token = jwt.encode({'uid': user.uid, 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=1440)}, app.config['SECRET_KEY'])

            output = {
                'token': token.decode('UTF-8'),
                'user_id': user.uid,
                'user_name': user.name,
                'public_id': user.public_id,
                'admin': user.admin
            }
            return response.data("Login successful", output)

        return response.error(401, 'Wrong user criental')

class LoginToken(Resource):
    method_decorators = [tokenize]

    def get(self, current_user):
        response = Response()
        return response.data("Token is valid", current_user.name)