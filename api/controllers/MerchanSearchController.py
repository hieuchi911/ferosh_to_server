from flask import request
from flask_restful import Resource
from api.helpers.MerchanSearchHandler import MerchanSearch
from api.helpers.ResponseHandler import Response
import json

search = MerchanSearch()
response = Response()
class MerchanSearch(Resource):
    def post(self):
        response = Response()

        page = request.form.get('page', default=1, type=int)
        limit = request.form.get('limit', default=15, type=int)
        search_string = request.form.get('search_string', default=0, type=str).lower()
        brand = request.form.get('brand_id', default="", type=int)
        cat = request.form.get('cat', default="", type=str).split(",")
        size = request.form.get('size', default="", type=str).split(",")
        session_id = request.form.get('sid', default=1)
        order = request.form.get('order', default="", type=str)
        order_dir = request.form.get('dir', default="", type=str)
        prices = request.form.get('prices', default="", type=str).split(",")
        if search_string:
            # Detect entities in search_string:
            detected_entities = search.detect_search_keywords(search_string)

            # generate json docs
            if order == "price":
                json_docs = search.generateJsonDocSearch(page, limit, merchan_name=brand, chosen_cat=cat, chosen_size=size,
                                                         detected_entities=detected_entities, search_string=search_string,
                                                         chosen_price_range=prices, reversing=order_dir)
            else:
                json_docs = search.generateJsonDocSearch(page, limit, merchan_name=brand, chosen_cat=cat, chosen_size=size,
                                                         detected_entities=detected_entities, search_string=search_string,
                                                         chosen_price_range=prices)
            print("============")
            print(session_id)
            print(json.dumps(json_docs, indent=4, sort_keys=True))
            print("============")

            # With the detected entities and the original search string, search for products using ES:
            results, num, time_taken, max_score = search.searching(json_docs, "suggest_index")

            # Start adding search info and resulting products to log
            products = search.return_products(results, num, time_taken)
            search.add_products(products, session_id, search_string, detected_entities)
            return response.data(f"Detected some entities in search string: \"{search_string}\" and searched for those products", products)
        else:  # if search_string is empty, return nothing
            output = {}
            return response.data("Result", output)

class MerchanSuggester(Resource):    # start suggesting from the third character, called whenever a new character is typed
    def post(self):
        response = Response()
        string = request.form.get('search_string', default="", type=str).lower()
        brand = request.form.get('brand', default=0, type=int)

        if string:
            detect_entities = search.detect_search_keywords(string)
            suggestions_aggregated = search.suggest_1(string=string, searchsuggest=False, index_name="suggest_index", detected_entities=detect_entities, merchan_name=brand)
            return response.data("Suggestion results", suggestions_aggregated)
        else:
            output = {}
            return response.data("Suggests none", output)

class MerchanSearchSuggester(Resource):
    def post(self):
        response = Response()

        page = request.form.get('page', default=0, type=int)
        limit = request.form.get('limit', default=15, type=int)
        keyword = request.form.get('search_string', default="", type=str).lower()
        brand = request.form.get('brand_id', default=0, type=int)
        cat = request.form.get('cat', default="", type=str).split(",")
        size = request.form.get('size', default="", type=str).split(",")
        order = request.form.get('order', default="", type=str)
        order_dir = request.form.get('dir', default="", type=str)
        prices = request.form.get('prices', default="", type=str).split(",")
        if brand or cat or size:
            detected_entities = search.detect_search_keywords(keyword)
            # With the detected entities and the original search string, search for products using ES:
            results = search.suggest_1(page=page, limit=limit, index_name="suggest_index", string=keyword,
                                       detected_entities=detected_entities, chosen_cat=cat, merchan_name=brand,
                                       chosen_size=size, chosen_price_range=prices, searchsuggest=True, reversing=order_dir)
            if order == "price":
                products = search.return_products(results, results["hits"]["total"], results["took"])
            else:
                products = search.return_products(results, results["hits"]["total"], results["took"])
            #search.add_products(products, session_id, search_string, detected_entities)
            return response.data(f"Search from suggestion: ", products)
        else:    # if search_string is empty, return nothing
            output = {}
            return response.data("Result", output)
