# -*- coding: utf-8 -*-
from datetime import date
from elasticsearch import Elasticsearch
from elasticsearch import helpers
from flask import request, jsonify
from flask_restful import Resource
from api.helpers.ResponseHandler import Response
from api.helpers.SearchHandler import Search
import json

es = Elasticsearch('localhost:9200')
search = Search()

es_index_log = "log_action_index"
es_doc_type_log = "log_action"

#print("AT SEARCH CONTROLLER LEVEL\nsearch cat name list at index 206: \n", search.category_name_list)

class Searcher(Resource):
    def post(self):
        response = Response()

        page = request.form.get('page', default=1, type=int)
        limit = request.form.get('limit', default=15, type=int)
        search_string = request.form.get('search_string', default=0, type=str).lower()
        brand = request.form.get('brand', default="", type=str).split(",")
        cat = request.form.get('cat', default="", type=str).split(",")
        size = request.form.get('size', default="", type=str).split(",")
        session_id = request.form.get('sid', default=1)
        order = request.form.get('order', default="", type=str)
        order_dir = request.form.get('dir', default="", type=str)
        prices = request.form.get('prices', default="", type=str).split(",")
        if search_string:
            # Detect entities in search_string:
            detected_entities = search.detect_search_keywords(search_string)

            # generate json docs
            if order == "price":
                json_docs = search.generateJsonDocSearch(page, limit, chosen_brand=brand, chosen_cat=cat, chosen_size=size, detected_entities=detected_entities, search_string=search_string, chosen_price_range=prices, reversing=order_dir)
            else:
                json_docs = search.generateJsonDocSearch(page, limit, chosen_brand=brand, chosen_cat=cat, chosen_size=size, detected_entities=detected_entities, search_string=search_string, chosen_price_range=prices)
            print ("============")
            print (session_id)
            print (json.dumps(json_docs, indent=4, sort_keys=True))
            print ("============")

            # With the detected entities and the original search string, search for products using ES:
            results, num, time_taken, max_score = search.searching(json_docs, "suggest_index")

            # Start adding search info and resulting products to log
            products = search.return_products(results, num, time_taken)
            search.add_products(products, session_id, search_string, detected_entities)

            return response.data(f"Detected some entities in search string: \"{search_string}\" and searched for those products", products)
        else:    # if search_string is empty, return nothing
            output = {}
            return response.data("Result", output)

class SearchSuggester(Resource):
    def post(self):
        response = Response()

        page = request.form.get('page', default=0, type=int)
        limit = request.form.get('limit', default=15, type=int)
        keyword = request.form.get('search_string', default="", type=str).lower()
        brand = request.form.get('brand', default="", type=str).split(",")
        cat = request.form.get('cat', default="", type=str).split(",")
        size = request.form.get('size', default="", type=str).split(",")
        order = request.form.get('order', default="", type=str)
        order_dir = request.form.get('dir', default="", type=str)
        prices = request.form.get('prices', default="", type=str).split(",")
        first_chosen = request.form.get('first', default="", type=str)
        if brand or cat or size:
            detected_entities = search.detect_search_keywords(keyword)
            # With the detected entities and the original search string, search for products using ES:
            results = search.suggest_1(page=page, limit=limit, index_name="suggest_index", string=keyword, detected_entities=detected_entities, chosen_cat=cat, chosen_brand=brand, chosen_size=size, chosen_price_range=prices, searchsuggest=True, first_chosen=first_chosen, reversing=order_dir)
            if order == "price":
                products = search.return_products(results, results["hits"]["total"], results["took"], reversing=order_dir)
            else:
                products = search.return_products(results, results["hits"]["total"], results["took"])
            #search.add_products(products, session_id, search_string, detected_entities)
            return response.data(f"Search from suggestion: ", products)
        else:    # if search_string is empty, return nothing
            output = {}
            return response.data("Result", output)

class ProductClick(Resource):
    def post(self):
        response = Response()
        sid = request.form.get('sid', type=str)

        # print ("======")
        # print (sid)
        # print ("======")

        try:
            # Check session query id exits
            info = es.get(index=es_index_log, doc_type=es_doc_type_log, id=sid)
            helpers.bulk(
                es,
                [
                    {
                        "_index": es_index_log,
                        '_op_type': 'update',
                        "_type": es_doc_type_log,
                        "_id": info["_id"],
                        "script": {
                            "inline": "ctx._source.total_clicks += 1"
                        }
                    }
                ],
                chunk_size=200,
                request_timeout=60
            )

            return response.message(200, "Update click by sid success")
        except Exception as f:
            return response.error(500, "Error with update click by sid " + sid)


class ProductView(Resource):    # onView product
    def post(self):
        response = Response()
        sid = request.form.get('sid', type=str)
        pids = len(request.form.get('pids', type=str).split(","))

        try:
            # Check session query id exits
            info = es.get(index=es_index_log, doc_type=es_doc_type_log, id=sid)
            helpers.bulk(
                es,
                [
                    {
                        "_index": es_index_log,
                        '_op_type': 'update',
                        "_type": es_doc_type_log,
                        "_id": info["_id"],
                        "script": {
                            "inline": "ctx._source.total_views += "+str(pids)+""
                        }
                    }
                ],
                chunk_size=200,
                request_timeout=60
            )

            return response.message(200, "Update views by sid success")
        except Exception as f:
            return response.error(500, "Error with update views by sid " + sid)

class Suggester(Resource):    # start suggesting from the third character, called whenever a new character is typed
    def post(self):
        response = Response()
        string = request.form.get('search_string', type=str).lower()

        if string:
            detect_entities = search.detect_search_keywords(string)
            suggestions_aggregated = search.suggest_1(string=string, searchsuggest=False, index_name="suggest_index", detected_entities=detect_entities)
            return response.data("Suggestion results", suggestions_aggregated)
        else:
            output = {}
            return response.data("Suggests none", output)

class HotSearchesSuggester(Resource):
    def post(self):
        response = Response()

        hot_keys = search.return_hot_keys()

        return response.data(f"Search from suggestion: ", hot_keys)
