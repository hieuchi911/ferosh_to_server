from flask import Flask
from flask_cors import CORS

app = Flask(__name__)
app.config['SECRET_KEY'] = '2stc2YK92DRgufjCfsFeizf80qP9CyIv'
app.config['JSON_SORT_KEYS'] = False
CORS(app)

from systems.database.mysql import configDB
db = configDB(app)

from api.routes import *
app = configRT(app)
