from flask_restful import Api

from api.controllers.LoginController import *
from api.controllers.UserController import *
from api.controllers.MerchanSearchController import *
from api.controllers.ActionLogController import *
from api.controllers.DictionariesController import *

def configRT(app):

    api = Api(app)

    # Login Route
    api.add_resource(Login, '/login')
    api.add_resource(LoginToken, '/login/status')

    # User Route
    # api.add_resource(UserAll, '/user')
    # api.add_resource(UserSession, '/user/session')

    # Search Route
    api.add_resource(Searcher, '/searchproduct')
    api.add_resource(ProductClick, "/logClicks")
    api.add_resource(ProductView, "/logViews")
    api.add_resource(Suggester, "/suggest")
    api.add_resource(SearchSuggester, "/searchsuggest")
    api.add_resource(HotSearchesSuggester, "/hotkeys")

    # Log display Route
    api.add_resource(LogDisplayer, "/displaylog")
    api.add_resource(LogExporter, "/exportlog")

    # Dictionary api Route
    api.add_resource(DictDisplayer, "/displaydicts")
    api.add_resource(UpdateAlternative, "/updatealt")
    api.add_resource(AddKeyword, "/addkey")

    # Merchandise search api Route
    api.add_resource(MerchanSearch, "/merchansearch")
    api.add_resource(MerchanSuggester, "/merchansuggest")
    api.add_resource(MerchanSearchSuggester, "/merchansearchsuggest")

    return app
