# Build Simple RestAPI Python [MVC]
Flask, Flask_RESTFul, Flask_SQLAlchemy, Flask-Migrate, flask_cors, jwt, mysql-python, mysqlclient, PyJWT, marshmallow

## Migration database
flask db init
flask db migrate -m "Initial migration."
flask db upgrade

## Run
python app.py